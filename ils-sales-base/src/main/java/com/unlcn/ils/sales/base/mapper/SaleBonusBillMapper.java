package com.unlcn.ils.sales.base.mapper;

import com.unlcn.ils.sales.base.model.SaleBonusBill;
import com.unlcn.ils.sales.base.model.SaleBonusBillExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SaleBonusBillMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sale_bonus_bill
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    int countByExample(SaleBonusBillExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sale_bonus_bill
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    int deleteByExample(SaleBonusBillExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sale_bonus_bill
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sale_bonus_bill
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    int insert(SaleBonusBill record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sale_bonus_bill
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    int insertSelective(SaleBonusBill record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sale_bonus_bill
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    List<SaleBonusBill> selectByExample(SaleBonusBillExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sale_bonus_bill
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    SaleBonusBill selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sale_bonus_bill
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    int updateByExampleSelective(@Param("record") SaleBonusBill record, @Param("example") SaleBonusBillExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sale_bonus_bill
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    int updateByExample(@Param("record") SaleBonusBill record, @Param("example") SaleBonusBillExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sale_bonus_bill
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    int updateByPrimaryKeySelective(SaleBonusBill record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sale_bonus_bill
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    int updateByPrimaryKey(SaleBonusBill record);
}