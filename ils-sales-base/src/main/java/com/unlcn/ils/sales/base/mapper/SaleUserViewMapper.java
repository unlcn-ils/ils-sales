package com.unlcn.ils.sales.base.mapper;

import com.unlcn.ils.sales.base.model.OrderViewDAO;
import com.unlcn.ils.sales.base.model.UserViewDAO;

import java.util.Map;
import java.util.Objects;

/**
 * Created by houjianhui on 2017/6/13.
 */
public interface SaleUserViewMapper {
    /**
     * 查询用户信息
     *
     * @param map 查询条件
     * @return
     */
    UserViewDAO getUserByCondition(Map<String, Object> map);
}
