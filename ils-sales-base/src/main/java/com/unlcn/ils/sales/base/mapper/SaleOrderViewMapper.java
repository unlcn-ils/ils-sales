package com.unlcn.ils.sales.base.mapper;

import com.unlcn.ils.sales.base.model.OrderSkuViewDAO;
import com.unlcn.ils.sales.base.model.OrderViewDAO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by houjianhui on 2017/6/7.
 */
public interface SaleOrderViewMapper {

    /**
     *  查询销售订单信息
     *
     * @param map 查询条件
     * @return
     */
    List<OrderViewDAO> listSaleOrder(Map<String, Object> map);

    /**
     *  统计销售订单
     *
     * @param map 查询条件
     * @return
     */
    int countSaleOrder(Map<String, Object> map);

    /**
     * 查询详情
     *
     * @param map
     * @return
     */
    OrderViewDAO getSaleOrderDetail(Map<String, Object> map);

    /**
     * 批量更新审核信息
     *
     * @param map 参数
     */
    void updateAuditStatus(Map<String, Object> map);

    /**
     * 更新订单状态
     *
     * @param map
     */
    void updateOrderStatus(Map<String, Object> map);

    List<OrderSkuViewDAO> listSaleOrderSku(@Param("id")Integer id);
}
