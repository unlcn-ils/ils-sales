package com.unlcn.ils.sales.base.mapper;

import com.unlcn.ils.sales.base.model.SaleVersionNote;
import org.apache.ibatis.annotations.Param;

/**
 * Created by houjianhui on 2017/9/18.
 */
public interface SaleVersionNoteViewMapper {

    SaleVersionNote selectLatestVersionNote(@Param("platform") String platform, @Param("apptype") Integer apptype);
}
