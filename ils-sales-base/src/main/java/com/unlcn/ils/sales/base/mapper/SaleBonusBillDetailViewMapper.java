package com.unlcn.ils.sales.base.mapper;

import com.unlcn.ils.sales.base.model.BillDetailViewDAO;

import java.util.List;
import java.util.Map;

/**
 * Created by houjianhui on 2017/6/8.
 */
public interface SaleBonusBillDetailViewMapper {
    /**
     * 查询账单明细
     *
     * @param map 查询条件
     * @return
     */
    List<BillDetailViewDAO> listBillDetail(Map<String, Object> map);

    /**
     * 统计账单明细
     *
     * @param map 查询条件
     * @return
     */
    Integer countBillDetail(Map<String, Object> map);
}
