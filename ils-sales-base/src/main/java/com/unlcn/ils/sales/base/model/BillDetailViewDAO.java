package com.unlcn.ils.sales.base.model;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by houjianhui on 2017/6/8.
 */
public class BillDetailViewDAO {
    private Integer carAmount;//商品车车辆数
    private BigDecimal saleOrderBonus;//销售订单奖金
    private Integer saleBonusBillId;//账单ID
    private Integer saleOrderId;//销售订单ID
    private String saleOrderCode;//销售订单编码

    private String departRegionName;//起始地
    private String destRegionName;//目的地
    private Date pickDate;//提车时间
    private Integer seriesAmt; //商品车数量

    public Integer getCarAmount() {
        return carAmount;
    }

    public void setCarAmount(Integer carAmount) {
        this.carAmount = carAmount;
    }

    public BigDecimal getSaleOrderBonus() {
        return saleOrderBonus;
    }

    public void setSaleOrderBonus(BigDecimal saleOrderBonus) {
        this.saleOrderBonus = saleOrderBonus;
    }

    public Integer getSaleBonusBillId() {
        return saleBonusBillId;
    }

    public void setSaleBonusBillId(Integer saleBonusBillId) {
        this.saleBonusBillId = saleBonusBillId;
    }

    public Integer getSaleOrderId() {
        return saleOrderId;
    }

    public void setSaleOrderId(Integer saleOrderId) {
        this.saleOrderId = saleOrderId;
    }

    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    public String getDepartRegionName() {
        return departRegionName;
    }

    public void setDepartRegionName(String departRegionName) {
        this.departRegionName = departRegionName;
    }

    public String getDestRegionName() {
        return destRegionName;
    }

    public void setDestRegionName(String destRegionName) {
        this.destRegionName = destRegionName;
    }

    public Date getPickDate() {
        return pickDate;
    }

    public void setPickDate(Date pickDate) {
        this.pickDate = pickDate;
    }

    public Integer getSeriesAmt() {
        return seriesAmt;
    }

    public void setSeriesAmt(Integer seriesAmt) {
        this.seriesAmt = seriesAmt;
    }

    @Override
    public String toString() {
        return "BillDetailViewDAO{" +
                "carAmount=" + carAmount +
                ", saleOrderBonus=" + saleOrderBonus +
                ", saleBonusBillId=" + saleBonusBillId +
                ", saleOrderId=" + saleOrderId +
                ", saleOrderCode='" + saleOrderCode + '\'' +
                ", departRegionName='" + departRegionName + '\'' +
                ", destRegionName='" + destRegionName + '\'' +
                ", pickDate=" + pickDate +
                ", seriesAmt=" + seriesAmt +
                '}';
    }
}
