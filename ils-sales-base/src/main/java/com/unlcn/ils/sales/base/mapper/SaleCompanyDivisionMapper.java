package com.unlcn.ils.sales.base.mapper;

import com.unlcn.ils.sales.base.model.SaleCompanyDivision;
import com.unlcn.ils.sales.base.model.SaleCompanyDivisionExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SaleCompanyDivisionMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sale_company_division
     *
     * @mbggenerated Fri Jun 09 13:30:55 CST 2017
     */
    int countByExample(SaleCompanyDivisionExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sale_company_division
     *
     * @mbggenerated Fri Jun 09 13:30:55 CST 2017
     */
    int deleteByExample(SaleCompanyDivisionExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sale_company_division
     *
     * @mbggenerated Fri Jun 09 13:30:55 CST 2017
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sale_company_division
     *
     * @mbggenerated Fri Jun 09 13:30:55 CST 2017
     */
    int insert(SaleCompanyDivision record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sale_company_division
     *
     * @mbggenerated Fri Jun 09 13:30:55 CST 2017
     */
    int insertSelective(SaleCompanyDivision record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sale_company_division
     *
     * @mbggenerated Fri Jun 09 13:30:55 CST 2017
     */
    List<SaleCompanyDivision> selectByExample(SaleCompanyDivisionExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sale_company_division
     *
     * @mbggenerated Fri Jun 09 13:30:55 CST 2017
     */
    SaleCompanyDivision selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sale_company_division
     *
     * @mbggenerated Fri Jun 09 13:30:55 CST 2017
     */
    int updateByExampleSelective(@Param("record") SaleCompanyDivision record, @Param("example") SaleCompanyDivisionExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sale_company_division
     *
     * @mbggenerated Fri Jun 09 13:30:55 CST 2017
     */
    int updateByExample(@Param("record") SaleCompanyDivision record, @Param("example") SaleCompanyDivisionExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sale_company_division
     *
     * @mbggenerated Fri Jun 09 13:30:55 CST 2017
     */
    int updateByPrimaryKeySelective(SaleCompanyDivision record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sale_company_division
     *
     * @mbggenerated Fri Jun 09 13:30:55 CST 2017
     */
    int updateByPrimaryKey(SaleCompanyDivision record);
}