package com.unlcn.ils.sales.base.model;

import java.io.Serializable;
import java.util.Date;

public class SaleOrderTicket implements Serializable {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sale_order_ticket.id
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    private Integer id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sale_order_ticket.order_id
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    private Integer orderId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sale_order_ticket.type
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    private String type;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sale_order_ticket.name
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    private String name;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sale_order_ticket.identify_number
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    private String identifyNumber;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sale_order_ticket.opening_bank
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    private String openingBank;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sale_order_ticket.account_number
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    private String accountNumber;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sale_order_ticket.address
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    private String address;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sale_order_ticket.contact_number
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    private String contactNumber;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sale_order_ticket.content
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    private String content;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sale_order_ticket.status
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    private String status;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sale_order_ticket.comment
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    private String comment;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sale_order_ticket.create_user
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    private String createUser;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sale_order_ticket.modified_user
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    private String modifiedUser;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sale_order_ticket.gmt_create
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    private Date gmtCreate;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sale_order_ticket.gmt_modified
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    private Date gmtModified;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table sale_order_ticket
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    private static final long serialVersionUID = 1L;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sale_order_ticket.id
     *
     * @return the value of sale_order_ticket.id
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sale_order_ticket.id
     *
     * @param id the value for sale_order_ticket.id
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sale_order_ticket.order_id
     *
     * @return the value of sale_order_ticket.order_id
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    public Integer getOrderId() {
        return orderId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sale_order_ticket.order_id
     *
     * @param orderId the value for sale_order_ticket.order_id
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sale_order_ticket.type
     *
     * @return the value of sale_order_ticket.type
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    public String getType() {
        return type;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sale_order_ticket.type
     *
     * @param type the value for sale_order_ticket.type
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sale_order_ticket.name
     *
     * @return the value of sale_order_ticket.name
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    public String getName() {
        return name;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sale_order_ticket.name
     *
     * @param name the value for sale_order_ticket.name
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sale_order_ticket.identify_number
     *
     * @return the value of sale_order_ticket.identify_number
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    public String getIdentifyNumber() {
        return identifyNumber;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sale_order_ticket.identify_number
     *
     * @param identifyNumber the value for sale_order_ticket.identify_number
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    public void setIdentifyNumber(String identifyNumber) {
        this.identifyNumber = identifyNumber == null ? null : identifyNumber.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sale_order_ticket.opening_bank
     *
     * @return the value of sale_order_ticket.opening_bank
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    public String getOpeningBank() {
        return openingBank;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sale_order_ticket.opening_bank
     *
     * @param openingBank the value for sale_order_ticket.opening_bank
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    public void setOpeningBank(String openingBank) {
        this.openingBank = openingBank == null ? null : openingBank.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sale_order_ticket.account_number
     *
     * @return the value of sale_order_ticket.account_number
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sale_order_ticket.account_number
     *
     * @param accountNumber the value for sale_order_ticket.account_number
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber == null ? null : accountNumber.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sale_order_ticket.address
     *
     * @return the value of sale_order_ticket.address
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    public String getAddress() {
        return address;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sale_order_ticket.address
     *
     * @param address the value for sale_order_ticket.address
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sale_order_ticket.contact_number
     *
     * @return the value of sale_order_ticket.contact_number
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    public String getContactNumber() {
        return contactNumber;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sale_order_ticket.contact_number
     *
     * @param contactNumber the value for sale_order_ticket.contact_number
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber == null ? null : contactNumber.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sale_order_ticket.content
     *
     * @return the value of sale_order_ticket.content
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    public String getContent() {
        return content;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sale_order_ticket.content
     *
     * @param content the value for sale_order_ticket.content
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sale_order_ticket.status
     *
     * @return the value of sale_order_ticket.status
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    public String getStatus() {
        return status;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sale_order_ticket.status
     *
     * @param status the value for sale_order_ticket.status
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sale_order_ticket.comment
     *
     * @return the value of sale_order_ticket.comment
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    public String getComment() {
        return comment;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sale_order_ticket.comment
     *
     * @param comment the value for sale_order_ticket.comment
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    public void setComment(String comment) {
        this.comment = comment == null ? null : comment.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sale_order_ticket.create_user
     *
     * @return the value of sale_order_ticket.create_user
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sale_order_ticket.create_user
     *
     * @param createUser the value for sale_order_ticket.create_user
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sale_order_ticket.modified_user
     *
     * @return the value of sale_order_ticket.modified_user
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    public String getModifiedUser() {
        return modifiedUser;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sale_order_ticket.modified_user
     *
     * @param modifiedUser the value for sale_order_ticket.modified_user
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    public void setModifiedUser(String modifiedUser) {
        this.modifiedUser = modifiedUser == null ? null : modifiedUser.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sale_order_ticket.gmt_create
     *
     * @return the value of sale_order_ticket.gmt_create
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    public Date getGmtCreate() {
        return gmtCreate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sale_order_ticket.gmt_create
     *
     * @param gmtCreate the value for sale_order_ticket.gmt_create
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sale_order_ticket.gmt_modified
     *
     * @return the value of sale_order_ticket.gmt_modified
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    public Date getGmtModified() {
        return gmtModified;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sale_order_ticket.gmt_modified
     *
     * @param gmtModified the value for sale_order_ticket.gmt_modified
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sale_order_ticket
     *
     * @mbggenerated Tue Sep 12 10:11:47 CST 2017
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", orderId=").append(orderId);
        sb.append(", type=").append(type);
        sb.append(", name=").append(name);
        sb.append(", identifyNumber=").append(identifyNumber);
        sb.append(", openingBank=").append(openingBank);
        sb.append(", accountNumber=").append(accountNumber);
        sb.append(", address=").append(address);
        sb.append(", contactNumber=").append(contactNumber);
        sb.append(", content=").append(content);
        sb.append(", status=").append(status);
        sb.append(", comment=").append(comment);
        sb.append(", createUser=").append(createUser);
        sb.append(", modifiedUser=").append(modifiedUser);
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", gmtModified=").append(gmtModified);
        sb.append("]");
        return sb.toString();
    }
}