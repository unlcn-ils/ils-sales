package com.unlcn.ils.sales.base.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class SaleBonusTurnover implements Serializable {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sale_bonus_turnover.id
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    private Integer id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sale_bonus_turnover.user_id
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    private Integer userId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sale_bonus_turnover.user_name
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    private String userName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sale_bonus_turnover.sale_order_id
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    private Integer saleOrderId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sale_bonus_turnover.sale_order_bonus
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    private BigDecimal saleOrderBonus;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sale_bonus_turnover.pay_type
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    private Integer payType;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sale_bonus_turnover.status
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    private Integer status;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sale_bonus_turnover.delete_mark
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    private Boolean deleteMark;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sale_bonus_turnover.gmt_create
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    private Date gmtCreate;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sale_bonus_turnover.gmt_update
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    private Date gmtUpdate;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table sale_bonus_turnover
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    private static final long serialVersionUID = 1L;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sale_bonus_turnover.id
     *
     * @return the value of sale_bonus_turnover.id
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sale_bonus_turnover.id
     *
     * @param id the value for sale_bonus_turnover.id
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sale_bonus_turnover.user_id
     *
     * @return the value of sale_bonus_turnover.user_id
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sale_bonus_turnover.user_id
     *
     * @param userId the value for sale_bonus_turnover.user_id
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sale_bonus_turnover.user_name
     *
     * @return the value of sale_bonus_turnover.user_name
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    public String getUserName() {
        return userName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sale_bonus_turnover.user_name
     *
     * @param userName the value for sale_bonus_turnover.user_name
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sale_bonus_turnover.sale_order_id
     *
     * @return the value of sale_bonus_turnover.sale_order_id
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    public Integer getSaleOrderId() {
        return saleOrderId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sale_bonus_turnover.sale_order_id
     *
     * @param saleOrderId the value for sale_bonus_turnover.sale_order_id
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    public void setSaleOrderId(Integer saleOrderId) {
        this.saleOrderId = saleOrderId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sale_bonus_turnover.sale_order_bonus
     *
     * @return the value of sale_bonus_turnover.sale_order_bonus
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    public BigDecimal getSaleOrderBonus() {
        return saleOrderBonus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sale_bonus_turnover.sale_order_bonus
     *
     * @param saleOrderBonus the value for sale_bonus_turnover.sale_order_bonus
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    public void setSaleOrderBonus(BigDecimal saleOrderBonus) {
        this.saleOrderBonus = saleOrderBonus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sale_bonus_turnover.pay_type
     *
     * @return the value of sale_bonus_turnover.pay_type
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    public Integer getPayType() {
        return payType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sale_bonus_turnover.pay_type
     *
     * @param payType the value for sale_bonus_turnover.pay_type
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sale_bonus_turnover.status
     *
     * @return the value of sale_bonus_turnover.status
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sale_bonus_turnover.status
     *
     * @param status the value for sale_bonus_turnover.status
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sale_bonus_turnover.delete_mark
     *
     * @return the value of sale_bonus_turnover.delete_mark
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    public Boolean getDeleteMark() {
        return deleteMark;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sale_bonus_turnover.delete_mark
     *
     * @param deleteMark the value for sale_bonus_turnover.delete_mark
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    public void setDeleteMark(Boolean deleteMark) {
        this.deleteMark = deleteMark;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sale_bonus_turnover.gmt_create
     *
     * @return the value of sale_bonus_turnover.gmt_create
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    public Date getGmtCreate() {
        return gmtCreate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sale_bonus_turnover.gmt_create
     *
     * @param gmtCreate the value for sale_bonus_turnover.gmt_create
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sale_bonus_turnover.gmt_update
     *
     * @return the value of sale_bonus_turnover.gmt_update
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sale_bonus_turnover.gmt_update
     *
     * @param gmtUpdate the value for sale_bonus_turnover.gmt_update
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sale_bonus_turnover
     *
     * @mbggenerated Tue Jun 06 11:30:23 CST 2017
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", userId=").append(userId);
        sb.append(", userName=").append(userName);
        sb.append(", saleOrderId=").append(saleOrderId);
        sb.append(", saleOrderBonus=").append(saleOrderBonus);
        sb.append(", payType=").append(payType);
        sb.append(", status=").append(status);
        sb.append(", deleteMark=").append(deleteMark);
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", gmtUpdate=").append(gmtUpdate);
        sb.append("]");
        return sb.toString();
    }
}