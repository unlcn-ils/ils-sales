package com.unlcn.ils.sales.base.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by houjianhui on 2017/6/7.
 */
public class OrderViewDAO {


    // 销售订单表
    private Integer id;
    private String code;
    private Integer userId;
    private Date pickDate;
    private Integer brands;
    private Integer seriesAmt;
    private BigDecimal standardSalePrice;
    private BigDecimal standardUnitPrice;
    private BigDecimal suggestSalePrice;
    private BigDecimal finalSalePrice;
    private BigDecimal insuranceSalePrice;
    private BigDecimal insuranceUnitPrice;
    private BigDecimal finalPurchasePrice;
    private Integer orderStatus;
    private Integer attachAuditStatus;
    private Integer auditStatus;
    private Integer auditId;
    private Date gmtCreate;
    private Date gmtUpdate;
    private String comment;
    private String auditComment;
    private String attachAuditStatusText;

    // 销售订单地址表
    private String departRegionCode;
    private String departRegionName;
    private String departContact;
    private String departPhone;
    private String destRegionCode;
    private String destRegionName;
    private String destContact;
    private String destPhone;
    private BigDecimal distance;
    private String realName;

    // 销售订单商品车表
    private List<OrderSkuViewDAO> orderSkus;

    private List<OrderAttachViewDAO> attachs;

    // 是否开票
    private Boolean isTicket;
    // 运输方式
    private Integer transportType;
    // 是否上门提车
    private Boolean isPick;
    // 是否送车上门
    private Boolean isDeliv;
    // 提车详细地址
    private String departAddr;
    // 送车详细地址
    private String destAddr;
    // 提车时间
    private String pickTime;
    // 支付状态
    private Integer payStatus;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getPickDate() {
        return pickDate;
    }

    public void setPickDate(Date pickDate) {
        this.pickDate = pickDate;
    }

    public Integer getBrands() {
        return brands;
    }

    public void setBrands(Integer brands) {
        this.brands = brands;
    }

    public Integer getSeriesAmt() {
        return seriesAmt;
    }

    public void setSeriesAmt(Integer seriesAmt) {
        this.seriesAmt = seriesAmt;
    }

    public BigDecimal getStandardSalePrice() {
        return standardSalePrice;
    }

    public void setStandardSalePrice(BigDecimal standardSalePrice) {
        this.standardSalePrice = standardSalePrice;
    }

    public BigDecimal getStandardUnitPrice() {
        return standardUnitPrice;
    }

    public void setStandardUnitPrice(BigDecimal standardUnitPrice) {
        this.standardUnitPrice = standardUnitPrice;
    }

    public BigDecimal getSuggestSalePrice() {
        return suggestSalePrice;
    }

    public void setSuggestSalePrice(BigDecimal suggestSalePrice) {
        this.suggestSalePrice = suggestSalePrice;
    }

    public BigDecimal getFinalSalePrice() {
        return finalSalePrice;
    }

    public void setFinalSalePrice(BigDecimal finalSalePrice) {
        this.finalSalePrice = finalSalePrice;
    }

    public BigDecimal getFinalPurchasePrice() {
        return finalPurchasePrice;
    }

    public void setFinalPurchasePrice(BigDecimal finalPurchasePrice) {
        this.finalPurchasePrice = finalPurchasePrice;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDepartRegionCode() {
        return departRegionCode;
    }

    public void setDepartRegionCode(String departRegionCode) {
        this.departRegionCode = departRegionCode;
    }

    public String getDepartRegionName() {
        return departRegionName;
    }

    public void setDepartRegionName(String departRegionName) {
        this.departRegionName = departRegionName;
    }

    public String getDepartContact() {
        return departContact;
    }

    public void setDepartContact(String departContact) {
        this.departContact = departContact;
    }

    public String getDepartPhone() {
        return departPhone;
    }

    public void setDepartPhone(String departPhone) {
        this.departPhone = departPhone;
    }

    public String getDestRegionCode() {
        return destRegionCode;
    }

    public void setDestRegionCode(String destRegionCode) {
        this.destRegionCode = destRegionCode;
    }

    public String getDestRegionName() {
        return destRegionName;
    }

    public void setDestRegionName(String destRegionName) {
        this.destRegionName = destRegionName;
    }

    public String getDestContact() {
        return destContact;
    }

    public void setDestContact(String destContact) {
        this.destContact = destContact;
    }

    public String getDestPhone() {
        return destPhone;
    }

    public void setDestPhone(String destPhone) {
        this.destPhone = destPhone;
    }

    public BigDecimal getDistance() {
        return distance;
    }

    public void setDistance(BigDecimal distance) {
        this.distance = distance;
    }

    public List<OrderSkuViewDAO> getOrderSkus() {
        return orderSkus;
    }

    public void setOrderSkus(List<OrderSkuViewDAO> orderSkus) {
        this.orderSkus = orderSkus;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getAuditComment() {
        return auditComment;
    }

    public void setAuditComment(String auditComment) {
        this.auditComment = auditComment;
    }

    public BigDecimal getInsuranceSalePrice() {
        return insuranceSalePrice;
    }

    public void setInsuranceSalePrice(BigDecimal insuranceSalePrice) {
        this.insuranceSalePrice = insuranceSalePrice;
    }

    public BigDecimal getInsuranceUnitPrice() {
        return insuranceUnitPrice;
    }

    public void setInsuranceUnitPrice(BigDecimal insuranceUnitPrice) {
        this.insuranceUnitPrice = insuranceUnitPrice;
    }

    public Integer getAuditId() {
        return auditId;
    }

    public void setAuditId(Integer auditId) {
        this.auditId = auditId;
    }

    public Integer getAttachAuditStatus() {
        return attachAuditStatus;
    }

    public void setAttachAuditStatus(Integer attachAuditStatus) {
        this.attachAuditStatus = attachAuditStatus;
    }

    public String getAttachAuditStatusText() {
        return attachAuditStatusText;
    }

    public void setAttachAuditStatusText(String attachAuditStatusText) {
        this.attachAuditStatusText = attachAuditStatusText;
    }

    public List<OrderAttachViewDAO> getAttachs() {
        return attachs;
    }

    public void setAttachs(List<OrderAttachViewDAO> attachs) {
        this.attachs = attachs;
    }

    public Boolean getIsTicket() {
        return isTicket;
    }

    public void setIsTicket(Boolean isTicket) {
        this.isTicket = isTicket;
    }

    public Integer getTransportType() {
        return transportType;
    }

    public void setTransportType(Integer transportType) {
        this.transportType = transportType;
    }

    public Boolean getIsPick() {
        return isPick;
    }

    public void setIsPick(Boolean isPick) {
        this.isPick = isPick;
    }

    public Boolean getIsDeliv() {
        return isDeliv;
    }

    public void setIsDeliv(Boolean isDeliv) {
        this.isDeliv = isDeliv;
    }

    public String getDepartAddr() {
        return departAddr;
    }

    public void setDepartAddr(String departAddr) {
        this.departAddr = departAddr;
    }

    public String getDestAddr() {
        return destAddr;
    }

    public void setDestAddr(String destAddr) {
        this.destAddr = destAddr;
    }

    public String getPickTime() {
        return pickTime;
    }

    public void setPickTime(String pickTime) {
        this.pickTime = pickTime;
    }

    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    @Override
    public String toString() {
        return "OrderViewDAO{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", userId=" + userId +
                ", pickDate=" + pickDate +
                ", brands=" + brands +
                ", seriesAmt=" + seriesAmt +
                ", standardSalePrice=" + standardSalePrice +
                ", standardUnitPrice=" + standardUnitPrice +
                ", suggestSalePrice=" + suggestSalePrice +
                ", finalSalePrice=" + finalSalePrice +
                ", insuranceSalePrice=" + insuranceSalePrice +
                ", insuranceUnitPrice=" + insuranceUnitPrice +
                ", finalPurchasePrice=" + finalPurchasePrice +
                ", orderStatus=" + orderStatus +
                ", attachAuditStatus=" + attachAuditStatus +
                ", auditStatus=" + auditStatus +
                ", auditId=" + auditId +
                ", gmtCreate=" + gmtCreate +
                ", gmtUpdate=" + gmtUpdate +
                ", comment='" + comment + '\'' +
                ", auditComment='" + auditComment + '\'' +
                ", attachAuditStatusText='" + attachAuditStatusText + '\'' +
                ", departRegionCode='" + departRegionCode + '\'' +
                ", departRegionName='" + departRegionName + '\'' +
                ", departContact='" + departContact + '\'' +
                ", departPhone='" + departPhone + '\'' +
                ", destRegionCode='" + destRegionCode + '\'' +
                ", destRegionName='" + destRegionName + '\'' +
                ", destContact='" + destContact + '\'' +
                ", destPhone='" + destPhone + '\'' +
                ", distance=" + distance +
                ", realName='" + realName + '\'' +
                ", orderSkus=" + orderSkus +
                ", attachs=" + attachs +
                ", isTicket=" + isTicket +
                ", transportType=" + transportType +
                ", isPick=" + isPick +
                ", isDeliv=" + isDeliv +
                ", departAddr='" + departAddr + '\'' +
                ", destAddr='" + destAddr + '\'' +
                ", pickTime='" + pickTime + '\'' +
                ", payStatus=" + payStatus +
                '}';
    }
}
