package com.unlcn.ils.sales.base.model;

/**
 * Created by houjianhui on 2017/8/15.
 */
public class OrderAttachViewDAO {
    private Integer id;
    private String picKey;
    private Integer saleOrderId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPicKey() {
        return picKey;
    }

    public void setPicKey(String picKey) {
        this.picKey = picKey;
    }

    public Integer getSaleOrderId() {
        return saleOrderId;
    }

    public void setSaleOrderId(Integer saleOrderId) {
        this.saleOrderId = saleOrderId;
    }

    @Override
    public String toString() {
        return "OrderAttachViewDAO{" +
                "id=" + id +
                ", picKey='" + picKey + '\'' +
                ", saleOrderId=" + saleOrderId +
                '}';
    }
}
