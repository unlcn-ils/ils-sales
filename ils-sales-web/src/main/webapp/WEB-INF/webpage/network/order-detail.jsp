<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div style="margin: 15px;">
    <form class="layui-form">
        <div class="layui-form-item">
            <label class="layui-form-label">销售订单号</label>
            <div class="layui-input-block">
                <input type="text" readonly style="border: 0" value="${(result.data.code)}"  class="layui-input">
            </div>
        </div>
        <div class="layui-form-item" style="width:600px;height:50px;">
            <div style="float:left;width:300px;height:40px;">
                <label class="layui-form-label">发货人</label>
                <div class="layui-input-block">
                    <input type="text" readonly style="border: 0" value="${(result.data.departContact)}"  class="layui-input">
                </div>
            </div>
            <div style="float:left;width:300px;height:40px;">
                <label class="layui-form-label">联系电话</label>
                <div class="layui-input-block">
                    <input type="text" readonly style="border: 0" value="${(result.data.departPhone)}" class="layui-input">
                </div>
            </div>
        </div>
        <div class="layui-form-item" style="width:600px;height:50px;">
            <div style="float:left;width:300px;height:40px;">
                <label class="layui-form-label">收货人</label>
                <div class="layui-input-block">
                    <input type="text" readonly style="border: 0" value="${(result.data.destContact)}" class="layui-input">
                </div>
            </div>
            <div style="float:left;width:300px;height:40px;">
                <label class="layui-form-label">联系电话</label>
                <div class="layui-input-block">
                    <input type="text" readonly style="border: 0" value="${(result.data.destPhone)}" class="layui-input">
                </div>
            </div>
        </div>
        <div class="layui-form-item" style="width:600px;height:50px;">
            <div style="float:left;width:300px;height:40px;">
                <label class="layui-form-label">运输距离</label>
                <div class="layui-input-block">
                    <input type="text" readonly style="border: 0" value="${(result.data.distance)}" class="layui-input">
                </div>
            </div>
        </div>
        <div class="layui-form-item" style="width:600px;height:50px;">
            <div style="float:left;width:300px;height:40px;">
                <label class="layui-form-label">下单时间</label>
                <div class="layui-input-block">
                    <input type="text" readonly style="border: 0" value='<fmt:formatDate value="${(result.data.gmtCreate)}" pattern="yyyy-MM-dd"/>' class="layui-input">
                </div>
            </div>
            <div style="float:left;width:300px;height:40px;">
                <label class="layui-form-label">提车时间</label>
                <div class="layui-input-block">
                    <input type="text" readonly style="border: 0" value='<fmt:formatDate value="${(result.data.pickDate)}" pattern="yyyy-MM-dd"/>'  class="layui-input">
                </div>
            </div>
        </div>
        <fieldset class="layui-elem-field">
            <legend>商品车详情</legend>
            <div class="layui-field-box layui-form">
                <table class="layui-table admin-table">

                    <tbody id="cont">

                    </tbody>
                </table>
            </div>
        </fieldset>
        <!--模板-->

        <div class="layui-form-item" style="width:600px;height:50px;">
            <div style="float:left;width:300px;height:40px;">
                <label class="layui-form-label">标准价格</label>
                <div class="layui-input-block">
                    <input type="text" readonly style="border: 0" value="${(result.data.standardSalePrice)}" class="layui-input">
                </div>
            </div>
            <div style="float:left;width:300px;height:40px;">
                <label class="layui-form-label">建议售价</label>
                <div class="layui-input-block">
                    <input type="text" readonly style="border: 0" value="${(result.data.suggestSalePrice)}" class="layui-input">
                </div>
            </div>
        </div>
        <button lay-filter="edit" style="display: none;"></button>
    </form>
</div>








