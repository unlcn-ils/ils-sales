<%--
  Created by IntelliJ IDEA.
  User: zhaoguixin
  Date: 2017/6/19
  Time: 下午2:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<html>
<head>
    <title>销售订单</title>
    <link rel="stylesheet" href="<%=path%>/plugins/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="<%=path%>/css/global.css" media="all">
    <link rel="stylesheet" href="<%=path%>/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<%=path%>/css/table.css"/>
</head>
<body>
<input type="hidden" id="rootpath" value="<%=basePath%>">
<input type="hidden" id="path" value="<%=path%>">
<div class="admin-main">
    <blockquote class="layui-elem-quote">
        <a href="javascript:;" class="layui-btn layui-btn-radius" id="query">
            <i class="layui-icon">&#xe615;</i> 搜索
        </a>

        <a href="javascript:;" class="layui-btn layui-btn-normal layui-btn-radius" id="audit">
            <i class="layui-icon">&#xe642;</i> 审核
        </a>
    </blockquote>
    <div style="margin: 15px; display: none;" id="showHideForm">
        <form class="layui-form" id="myForm">
            <div class="layui-form-item">
                <div class="layui-inline">
                        <label class="layui-form-label">起运地</label>
                        <div class="layui-input-block">
                            <select id="departRegionCode" name="departRegionCode" lay-verify="required" lay-search=""   lay-filter="province">
                                <option value="">--请选择-</option>
                                <c:forEach items="${result.data}" var="dep">
                                <option value="${dep.id}">${dep.sname}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                <div class="layui-inline">
                    <label class="layui-form-label">目的省</label>
                    <div class="layui-input-block">
                        <select name="destRegionProvince" id="destRegionProvince" lay-search="" class="layui-select"  lay-filter="city">
                            <option value="">--选择一个起运地--</option>
                        </select>
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">目的市</label>
                    <div class="layui-input-block">
                        <select name="destRegionCode" id="destRegionCode" lay-search="" class="layui-select">
                            <option value="">--选择一个目的地省--</option>
                        </select>
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">销售订单号</label>
                    <div class="layui-input-block">
                        <input type="text" name="code" id="code" placeholder="请输入订单号" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">提车日期</label>
                     <div class="layui-input-block">
                        <div class="layui-input-inline">
                            <input class="layui-input"  name="pickDateBegin" id="pickDateBegin"  placeholder="开始日期" onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD 00:00:00'})">
                        </div>
                        <div class="layui-input-inline">
                            <input class="layui-input"  name="pickDateEnd" id="pickDateEnd"   placeholder="截止日期" onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD 00:00:00'})">
                        </div>
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">审批状态</label>
                    <div class="layui-input-block">
                        <select name="auditStatus" id="auditStatus" lay-search="" class="layui-select">
                            <option value="">--请选择--</option>
                            <option value="10">待审批</option>
                            <option value="20">已通过</option>
                            <option value="30">未通过</option>
                        </select>
                    </div>
                </div>
            </div>
        </form>
        <div class="layui-inline">
            <div class="layui-input-block">
                <button class="layui-btn" id="formsearch">查询</button>
                <button class="layui-btn" id="formreset">重置</button>
            </div>
        </div>
    </div>
</div>
<fieldset class="layui-elem-field">
    <legend>数据列表</legend>
    <div class="layui-field-box layui-form">
        <table class="layui-table admin-table">
            <thead>
            <tr>
                <th style="width: 30px;"><input type="checkbox" lay-filter="allselector" lay-skin="primary"></th>
                <th>销售订单号</th>
                <th>起运地</th>
                <th>目的地</th>
                <th>运输距离</th>
                <th>商品车数量</th>
                <th>标准价格</th>
                <th>建议售价</th>
                <th>提车时间</th>
                <th>审批状态</th>
                <th>下单人</th>
                <th>详情</th>
            </tr>
            </thead>
            <tbody id="content">
            </tbody>
        </table>
    </div>
</fieldset>
<div class="admin-table-page">
    <div id="paged" class="page">
    </div>
</div>
</div>

<!--模板-->
<script type="text/html" id="tpl">

    {{# layui.each(d.data.orderList, function(index, item){ }}
    <tr>
        <td><input type="checkbox" lay-skin="primary" value="{{ item.id }}" name="{{ item.orderAuditStatusText }}"></td>
        <td>{{ item.code }}</td>
        <td>{{ item.departRegionName }}</td>
        <td>{{ item.destRegionName }}</td>
        <td>{{ item.distance==null ? '':item.distance }}</td>
        <td>{{ item.seriesAmt }}</td>
        <td>{{ item.standardSalePrice}}</td>
        <td>{{ item.suggestSalePrice }}</td>
        <td>{{ item.pickDate }}</td>
        <td>{{ item.orderAuditStatusText }}</td>
        <td>{{ item.realName }}</td>
        <td>
            <a href="javascript:;" data-id="{{ item.id }}" data-name="{{ item.id }}" data-opt="edit" class="layui-btn layui-btn-mini">查看</a>
        </td>

    </tr>
    {{# }); }}
</script>

<script type="text/javascript" src="<%=path%>/plugins/layui/layui.js"></script>
<script>
    rootPath=document.getElementById("rootpath").value;
    path=document.getElementById("path").value;
    layui.config({
        base: rootPath +'js/'
    });
    layui.use(['paging', 'form', 'baajax'], function () {
        <%--console.log(rootPath)--%>
        <%--console.log(path)--%>
        <%--console.log('<%=path%>')--%>
        var $ = layui.jquery,
            paging = layui.paging(),
            //layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
            layerTips = layui.layer,
            layer = layui.layer, //获取当前窗口的layer对象
            form = layui.form();
            baajax = layui.baajax;

        paging.init({
            openWait: true,
            url: 'list?v=' + new Date().getTime(), //地址
            elem: '#content', //内容容器
            params: { //发送到服务端的参数

            },
            type: 'POST',
            tempElem: '#tpl', //模块容器
            pageConfig: { //分页参数配置
                elem: '#paged', //分页容器
            },
            success: function () { //渲染成功的回调
                //alert('渲染成功');
            },
            fail: function (msg) { //获取数据失败的回调
                layer.msg(msg);
                //alert('获取数据失败')
            },
            complate: function (data) { //完成的回调
                //alert('处理完成');
//                console.log(data)
                //重新渲染复选框
                form.render('checkbox');
                form.on('checkbox(allselector)', function (data) {
                    var elem = data.elem;
                    $('#content').children('tr').each(function () {
                        var $that = $(this);
                        //全选或反选
                        $that.children('td').eq(0).children('input[type=checkbox]')[0].checked = elem.checked;
                        form.render('checkbox');
                    });
                });

                //绑定所有编辑按钮事件
                $('#content').children('tr').each(function( _idx , _val) {
                    var $that = $(this);
                    var _list = data.data.orderList[_idx].orderSkus;//这个就是需要渲染的list
                    $that.children('td:last-child').children('a[data-opt=edit]').on('click', function() {
                        $.get('detail',{orderId:$(this).data('id')}, function(form) {  //调用跳转到编辑页面
                            addBoxIndex = layer.open({
                                type: 1,
                                title: '详情',
//                                    elem: '#cont', //内容容器
//                                    tempElem: '#abd', //模块容器
                                content: form,
//                                    btn: ['保存', '取消'],
                                shade: false,
                                offset: ['50px', '20%'],
                                area: ['700px', '500px'],
                                zIndex: 19950942,
                                maxmin: true,
                                yes: function(index) {
                                    //触发表单的提交事件
                                    $('form.layui-form').find('button[lay-filter=edit]').click();
                                },
                                full: function(elem) {
                                    var win = window.top === window.self ? window : parent.window;
                                    $(win).on('resize', function() {
                                        var $this = $(this);
                                        elem.width($this.width()).height($this.height()).css({
                                            top: 0,
                                            left: 0
                                        });
                                        elem.children('div.layui-layer-content').height($this.height() - 95);
                                    });
                                },
                                success: function(layero, index) {
                                    var $cont = $("#cont");
                                    //在这个地方渲染_list 然后添加到这个 $cont 里面就行了
                                    var str = "";
                                    var html=''
                                    $.each(_list , function(i,v){
                                        /*console.log(v.brandName)
                                         console.log(v.brandName)*/
                                        html+='<tr>'+
                                            '<td><span class="nb">'+v.brandName+'</span>-<span class="nb">'+v.seriesName+'</span>-<span class="nb">'+v.modelCode+'</span></td>'+
                                            '<td>'+v.amt+'辆</td>'+
                                            '</tr>'

                                    })
                                    $("#cont").html(html);
                                },
                                end: function() {
                                    addBoxIndex = -1;
                                }
                            });
                        });
                    });
                });
                form.on('select(province)', function(data){
                    $.getJSON( rootPath + "/area/provincearea/"+data.value, function(data){
                        var optionstring = "";
                        $.each(data.data, function(i,item){
                            optionstring += "<option value=\"" + item.id + "\" >" + item.sname + "</option>";
                        });
                        $("#destRegionProvince").html('<option value=""></option>' + optionstring);
                        form.render('select'); //这个很重要
                    });
                });

                form.on('select(city)', function(data){
                    var departRegionCode = $('#departRegionCode').val();
                    $.getJSON(rootPath + "/area/destarea/"+ departRegionCode +"/"+data.value, function(data){
                        var optionstring = "";
                        $.each(data.data, function(i,item){
                            optionstring += "<option value=\"" + item.id + "\" >" + item.sname + "</option>";
                        });
                        $("#destRegionCode").html('<option value=""></option>' + optionstring);
                        form.render('select'); //这个很重要
                    });
                });

            },
        });


        //触发审核按钮
        $('#audit').on('click', function () {
            var ids ='' ;
            var tem =0;
            var num =0;
            $('#content').children('tr').each(function() {
                var $that = $(this);
                var $cbx = $that.children('td').eq(0).children('input[type=checkbox]')[0].checked;
                if($cbx) {
                    var n = $that.children('td').eq(0).children('input[type=checkbox]')[0].value;
                    var status = $that.children('td').eq(0).children('input[type=checkbox]')[0].name;
                    if(status == '待审批'){
                        ids += n + ',';
                        tem++;
                    } else {
                        num++;
                        return;
                    }
                }
            });
            if(tem==0 || num>0){
                layer.msg('请选择待审批的订单！');
                return;
            }
            ids = ids.substring(0,ids.length-1);
            //询问框
            layer.confirm('是否同意审批', {
                btn: ['同意','拒绝'] //按钮
            }, function(){

                $.get('audit',{ids:ids}, function(form) {  //调用批量删除
                    location.reload(); //刷新
                });
            }, function(){

                if(tem>1){
                    layer.msg('只能选择一条订单拒绝！');
                    return;
                }
//                var dataById = document.getElementById(ids[0]);
//                layer.msg(dataById);
                $.post('to-refuse', {id: ids}, function (form) {  //调用跳转到编辑页面
                    layer.open({
                        type: 1,
                        title: '审核意见',
                        content: form,
                        btn: ['确定', '取消'],
                        shade: false,
                        offset: ['100px', '30%'],
                        area: ['400px', '250px'],
                        zIndex: 19950924,
                        maxmin: true,
                        yes: function (index) {
                            //ajax提交新增数据
                            var comment = $("#comment").val();
                            baajax.post('update', {id: ids, comment: comment, auditStatus: 30 }, function (result) {
                                if (result.success) {   //如果成功关闭新增功能
                                    layerTips.close(index);
                                }
                                layer.msg(result.message, {zIndex: layer.zIndex}, function () {
                                    if (result.success) {   //如果成功关闭新增功能
                                        location.reload(); //刷新
                                    }
                                });
                            })

                        },
                        full: function (elem) {
                            var win = window.top === window.self ? window : parent.window;
                            $(win).on('resize', function () {
                                var $this = $(this);
                                elem.width($this.width()).height($this.height()).css({
                                    top: 0,
                                    left: 0
                                });
                                elem.children('div.layui-layer-content').height($this.height() - 95);
                            });
                        },
                    });
                });
            });


        });

        //搜索按钮展开div
        $('#query').on('click', function () {
            $('#showHideForm').toggle();
        });


        //重置
        $('#formreset').on('click', function () {
            $('#myForm')[0].reset();
        });

        //查询
        $('#formsearch').on('click', function () {
            var code =  $('#code').val();
            var departRegionCode = $('#departRegionCode').val();
            var destRegionCode = $('#destRegionCode').val();
            var pickDateBegin = $('#pickDateBegin').val();
            var pickDateEnd = $('#pickDateEnd').val();
            var auditStatus = $('#auditStatus').val();
            paging.init({
                openWait: true,
                url: 'list?v=' + new Date().getTime(), //地址
                elem: '#content', //内容容器
                params: { //发送到服务端的参数
                    code: code, //获取输入的关键字。
                    departRegionCode : departRegionCode,
                    destRegionCode : destRegionCode,
                    pickDateBegin : pickDateBegin,
                    pickDateEnd : pickDateEnd,
                    auditStatus : auditStatus
                },
                type: 'POST',
                tempElem: '#tpl', //模块容器
                pageConfig: { //分页参数配置
                    elem: '#paged', //分页容器
                },
                success: function () { //渲染成功的回调
                    //alert('渲染成功');
                },
                fail: function (msg) { //获取数据失败的回调
                    layer.msg(msg);
                    //alert('获取数据失败')
                },
                complate: function (data) { //完成的回调
                    //alert('处理完成');
                    //重新渲染复选框
                    form.render('checkbox');
                    form.on('checkbox(allselector)', function (data) {
                        var elem = data.elem;
                        $('#content').children('tr').each(function () {
                            var $that = $(this);
                            //全选或反选
                            $that.children('td').eq(0).children('input[type=checkbox]')[0].checked = elem.checked;
                            form.render('checkbox');
                        });
                    });
                    //绑定所有编辑按钮事件
                    $('#content').children('tr').each(function( _idx , _val) {
                        var $that = $(this);
                        var _list = data.data.orderList[_idx].orderSkus;//这个就是需要渲染的list
                        $that.children('td:last-child').children('a[data-opt=edit]').on('click', function() {
                            $.get('detail',{orderId:$(this).data('id')}, function(form) {  //调用跳转到编辑页面
                                addBoxIndex = layer.open({
                                    type: 1,
                                    title: '详情',
                                    content: form,
                                    shade: false,
                                    offset: ['50px', '20%'],
                                    area: ['700px', '500px'],
                                    zIndex: 19950942,
                                    maxmin: true,
                                    yes: function(index) {
                                        //触发表单的提交事件
                                        $('form.layui-form').find('button[lay-filter=edit]').click();
                                    },
                                    full: function(elem) {
                                        var win = window.top === window.self ? window : parent.window;
                                        $(win).on('resize', function() {
                                            var $this = $(this);
                                            elem.width($this.width()).height($this.height()).css({
                                                top: 0,
                                                left: 0
                                            });
                                            elem.children('div.layui-layer-content').height($this.height() - 95);
                                        });
                                    },
                                    success: function(layero, index) {
                                        var $cont = $("#cont");
                                        //在这个地方渲染_list 然后添加到这个 $cont 里面就行了
                                        var str = "";
                                        var html=''
                                        $.each(_list , function(i,v){
                                            /*console.log(v.brandName)
                                             console.log(v.brandName)*/
                                            html+='<tr>'+
                                                '<td><span class="nb">'+v.brandName+'</span>-<span class="nb">'+v.seriesName+'</span>-<span class="nb">'+v.modelCode+'</span></td>'+
                                                '<td>'+v.amt+'辆</td>'+
                                                '</tr>'

                                        })
                                        $("#cont").html(html);
                                    },
                                    end: function() {
                                        addBoxIndex = -1;
                                    }
                                });
                            });
                        });
                    });
                },
            });
        });
    });
</script>

<script>
    layui.use('laydate', function(){
        var laydate = layui.laydate;

        var start = {
            min: '1999-06-16 23:59:59'
            ,max: '2099-06-16 23:59:59'
            ,istoday: false
            ,choose: function(datas){
                end.min = datas; //开始日选好后，重置结束日的最小日期
                end.start = datas //将结束日的初始值设定为开始日
            }
        };

        var end = {
            min: '1999-06-16 23:59:59'
            ,max: '2099-06-16 23:59:59'
            ,istoday: false
            ,choose: function(datas){
                start.max = datas; //结束日选好后，重置开始日的最大日期
            }
        };

    });
</script>
</body>
</html>
