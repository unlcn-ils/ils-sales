package com.unlcn.ils.sales.web.schedule;

import com.unlcn.ils.sales.backend.service.OrderService;
import com.unlcn.ils.sales.backend.service.OrderServiceImpl;

/**
 * 定时任务工厂类
 *
 */
public class QuartzClusterFactory {

    /**
     * 定时任务 销售订单服务接口
     *
     * @return OrderService
     */
    public static OrderService getOrderService() {
        return SpringApplication.getBean(OrderServiceImpl.class);
    }

}
