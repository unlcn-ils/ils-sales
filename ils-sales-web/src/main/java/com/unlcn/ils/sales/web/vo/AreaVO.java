package com.unlcn.ils.sales.web.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Created by houjianhui on 2017/6/9.
 */
@ApiModel(value = "AreaVO", description = "区域数据")
public class AreaVO {
    @ApiModelProperty(value = "主键ID，与编码相同")
    private Integer id;
    @ApiModelProperty(value = "区域编码")
    private String code;
    @ApiModelProperty(value = "区域名称")
    private String name;
    @ApiModelProperty(value = "区域简称")
    private String sname;
    @ApiModelProperty(value = "上级区域编码")
    private String parentCode;
    @ApiModelProperty(value = "路径，当前数据所有上级的ID路径，用逗号分隔，如：110000,110100,110101")
    private String path;
    @ApiModelProperty(value = "状态，10:正常、20:禁用")
    private String status;
    @ApiModelProperty(value = "区域等级，可选：10:省、20:市、30:县",allowableValues="10,20,30")
    private String level;
    @ApiModelProperty(value = "汉语拼音")
    private String pinyin;
    @ApiModelProperty(value = "拼音首字母")
    private String initialWords;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getPinyin() {
        return pinyin;
    }

    public void setPinyin(String pinyin) {
        this.pinyin = pinyin;
    }

    public String getInitialWords() {
        return initialWords;
    }

    public void setInitialWords(String initialWords) {
        this.initialWords = initialWords;
    }

    @Override
    public String toString() {
        return "AreaVO{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", sname='" + sname + '\'' +
                ", parentCode='" + parentCode + '\'' +
                ", path='" + path + '\'' +
                ", status='" + status + '\'' +
                ", level='" + level + '\'' +
                ", pinyin='" + pinyin + '\'' +
                ", initialWords='" + initialWords + '\'' +
                '}';
    }
}
