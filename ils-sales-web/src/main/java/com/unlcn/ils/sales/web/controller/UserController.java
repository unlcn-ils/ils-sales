package com.unlcn.ils.sales.web.controller;

import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.sales.backend.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by houjianhui on 2017/6/15.
 */
@Controller
@RequestMapping(value = "/user")
public class UserController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    /**
     * 加载销售人员列表
     *
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public ResultDTO<Object> list() {
        ResultDTO<Object> result = new ResultDTO<>(true, null, "销售人员列表加载成功");
        try {
            result.setData(userService.listUser());
        } catch (Exception e) {
            LOGGER.error("UserController.list error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

}
