package com.unlcn.ils.sales.web.schedule;

import com.unlcn.ils.sales.backend.service.OrderService;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

/**
 * 交车审核后运单完成定时任务
 *
 * @author hjh
 */
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
public class SaleOrderQuartzCluster implements Job, Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(SaleOrderQuartzCluster.class);

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        LOGGER.info("SaleOrderQuartzCluster.execute start");
        OrderService orderService = QuartzClusterFactory.getOrderService();

        try {
            orderService.updateSaleOrderStatusByKyleOrderStatus();
        } catch (Exception e) {
            LOGGER.error("SaleOrderQuartzCluster.execute error: {}", e);
        }
        LOGGER.info("SaleOrderQuartzCluster.execute end");
    }

}
