package com.unlcn.ils.sales.web.query;

import cn.huiyunche.commons.domain.PageVo;

/**
 * Created by houjianhui on 2017/6/8.
 */
public class OrderQuery {

    // 销售订单编号
    private String code;

    // 销售订单物流状态
    private Integer orderStatus;

    // 下单人
    private Integer userId;

    // 启运地
    private String departRegionCode;

    // 目的地
    private String destRegionCode;

    // 提车时间（开始）
    private String pickDateBegin;

    // 提车时间（结束）
    private String pickDateEnd;

    // 审核状态
    private String auditStatus;

    private PageVo pageVo;

    public PageVo getPageVo() {
        return pageVo;
    }

    public void setPageVo(PageVo pageVo) {
        this.pageVo = pageVo;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getDepartRegionCode() {
        return departRegionCode;
    }

    public void setDepartRegionCode(String departRegionCode) {
        this.departRegionCode = departRegionCode;
    }

    public String getDestRegionCode() {
        return destRegionCode;
    }

    public void setDestRegionCode(String destRegionCode) {
        this.destRegionCode = destRegionCode;
    }

    public String getPickDateBegin() {
        return pickDateBegin;
    }

    public void setPickDateBegin(String pickDateBegin) {
        this.pickDateBegin = pickDateBegin;
    }

    public String getPickDateEnd() {
        return pickDateEnd;
    }

    public void setPickDateEnd(String pickDateEnd) {
        this.pickDateEnd = pickDateEnd;
    }

    public String getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(String auditStatus) {
        this.auditStatus = auditStatus;
    }

    @Override
    public String toString() {
        return "OrderQuery{" +
                "code='" + code + '\'' +
                ", orderStatus=" + orderStatus +
                ", userId=" + userId +
                ", departRegionCode='" + departRegionCode + '\'' +
                ", destRegionCode='" + destRegionCode + '\'' +
                ", pickDateBegin=" + pickDateBegin +
                ", pickDateEnd=" + pickDateEnd +
                ", auditStatus=" + auditStatus +
                '}';
    }
}
