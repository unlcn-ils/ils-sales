package com.unlcn.ils.sales.backend.bo;

import java.math.BigDecimal;

/**
 * Created by houjianhui on 2017/6/9.
 */
public class FreightQueryBO {

    private BigDecimal distance;
    private BigDecimal insureFee;
    private BigDecimal freightUnit;
    private BigDecimal standardUnit;
    private BigDecimal freightComplete;
    private BigDecimal standardComplete;

    public BigDecimal getDistance() {
        return distance;
    }

    public void setDistance(BigDecimal distance) {
        this.distance = distance;
    }

    public BigDecimal getInsureFee() {
        return insureFee;
    }

    public void setInsureFee(BigDecimal insureFee) {
        this.insureFee = insureFee;
    }

    public BigDecimal getFreightUnit() {
        return freightUnit;
    }

    public void setFreightUnit(BigDecimal freightUnit) {
        this.freightUnit = freightUnit;
    }

    public BigDecimal getStandardUnit() {
        return standardUnit;
    }

    public void setStandardUnit(BigDecimal standardUnit) {
        this.standardUnit = standardUnit;
    }

    public BigDecimal getFreightComplete() {
        return freightComplete;
    }

    public void setFreightComplete(BigDecimal freightComplete) {
        this.freightComplete = freightComplete;
    }

    public BigDecimal getStandardComplete() {
        return standardComplete;
    }

    public void setStandardComplete(BigDecimal standardComplete) {
        this.standardComplete = standardComplete;
    }

    @Override
    public String toString() {
        return "FreightQueryBO{" +
                "distance=" + distance +
                ", insureFee=" + insureFee +
                ", freightUnit=" + freightUnit +
                ", standardUnit=" + standardUnit +
                ", freightComplete=" + freightComplete +
                ", standardComplete=" + standardComplete +
                '}';
    }
}
