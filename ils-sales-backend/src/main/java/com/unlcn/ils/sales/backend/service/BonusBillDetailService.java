package com.unlcn.ils.sales.backend.service;

import cn.huiyunche.commons.domain.PageVo;
import com.unlcn.ils.sales.backend.bo.BonusBillDetailBO;

import java.util.Map;

/**
 * Created by houjianhui on 2017/6/8.
 */
public interface BonusBillDetailService {

    /**
     * 保存销售账单明细
     *
     * @param bo 销售账单明细
     * @return
     * @throws Exception
     */
    Integer save(BonusBillDetailBO bo) throws Exception;

    /**
     * 查询销售账单明细
     *
     * @param pageVo 分页对象
     * @param billId 账单ID
     * @return
     * @throws Exception
     */
    Map<String, Object> listBonusBillDetail(PageVo pageVo, Integer billId) throws Exception;
}
