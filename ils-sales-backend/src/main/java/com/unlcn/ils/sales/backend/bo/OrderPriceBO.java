package com.unlcn.ils.sales.backend.bo;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by houjianhui on 2017/6/9.
 */
public class OrderPriceBO {

    // 标准售价
    private BigDecimal standardSalePrice;
    // 线路ID
    private Integer lineId;
    // 产品ID
    private Integer programId;
    // 里程
    private BigDecimal mileage;
    // 标准零担单价
    private BigDecimal ltlUnitPrice;
    // 标准零担总价
    private BigDecimal ltlGrossPrice;
    // 标准整车单价
    private BigDecimal tlUnitPrice;
    // 标准整车总价
    private BigDecimal tlGrossPrice;
    // 运输模式编码
    private String transModeCode;
    // 运输模式名称
    private String transModeName;
    // 运载工具ID
    private Integer carryToolId;
    // 运载工具名称
    private String carryToolName;
    // 保险单价
    private BigDecimal unitPrice;

    private BigDecimal insuranceSalePrice;

    private BigDecimal insuranceUnitPrice;

    private List<FeeBO> feeBOS;

    public BigDecimal getStandardSalePrice() {
        return standardSalePrice;
    }

    public void setStandardSalePrice(BigDecimal standardSalePrice) {
        this.standardSalePrice = standardSalePrice;
    }

    public List<FeeBO> getFeeBOS() {
        return feeBOS;
    }

    public void setFeeBOS(List<FeeBO> feeBOS) {
        this.feeBOS = feeBOS;
    }

    public Integer getLineId() {
        return lineId;
    }

    public void setLineId(Integer lineId) {
        this.lineId = lineId;
    }

    public Integer getProgramId() {
        return programId;
    }

    public void setProgramId(Integer programId) {
        this.programId = programId;
    }

    public BigDecimal getMileage() {
        return mileage;
    }

    public void setMileage(BigDecimal mileage) {
        this.mileage = mileage;
    }

    public BigDecimal getLtlUnitPrice() {
        return ltlUnitPrice;
    }

    public void setLtlUnitPrice(BigDecimal ltlUnitPrice) {
        this.ltlUnitPrice = ltlUnitPrice;
    }

    public BigDecimal getLtlGrossPrice() {
        return ltlGrossPrice;
    }

    public void setLtlGrossPrice(BigDecimal ltlGrossPrice) {
        this.ltlGrossPrice = ltlGrossPrice;
    }

    public BigDecimal getTlUnitPrice() {
        return tlUnitPrice;
    }

    public void setTlUnitPrice(BigDecimal tlUnitPrice) {
        this.tlUnitPrice = tlUnitPrice;
    }

    public BigDecimal getTlGrossPrice() {
        return tlGrossPrice;
    }

    public void setTlGrossPrice(BigDecimal tlGrossPrice) {
        this.tlGrossPrice = tlGrossPrice;
    }

    public String getTransModeCode() {
        return transModeCode;
    }

    public void setTransModeCode(String transModeCode) {
        this.transModeCode = transModeCode;
    }

    public String getTransModeName() {
        return transModeName;
    }

    public void setTransModeName(String transModeName) {
        this.transModeName = transModeName;
    }

    public Integer getCarryToolId() {
        return carryToolId;
    }

    public void setCarryToolId(Integer carryToolId) {
        this.carryToolId = carryToolId;
    }

    public String getCarryToolName() {
        return carryToolName;
    }

    public void setCarryToolName(String carryToolName) {
        this.carryToolName = carryToolName;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public BigDecimal getInsuranceSalePrice() {
        return insuranceSalePrice;
    }

    public void setInsuranceSalePrice(BigDecimal insuranceSalePrice) {
        this.insuranceSalePrice = insuranceSalePrice;
    }

    public BigDecimal getInsuranceUnitPrice() {
        return insuranceUnitPrice;
    }

    public void setInsuranceUnitPrice(BigDecimal insuranceUnitPrice) {
        this.insuranceUnitPrice = insuranceUnitPrice;
    }

    @Override
    public String toString() {
        return "OrderPriceBO{" +
                "standardSalePrice=" + standardSalePrice +
                ", lineId=" + lineId +
                ", programId=" + programId +
                ", mileage=" + mileage +
                ", ltlUnitPrice=" + ltlUnitPrice +
                ", ltlGrossPrice=" + ltlGrossPrice +
                ", tlUnitPrice=" + tlUnitPrice +
                ", tlGrossPrice=" + tlGrossPrice +
                ", transModeCode='" + transModeCode + '\'' +
                ", transModeName='" + transModeName + '\'' +
                ", carryToolId=" + carryToolId +
                ", carryToolName='" + carryToolName + '\'' +
                ", unitPrice=" + unitPrice +
                ", insuranceSalePrice=" + insuranceSalePrice +
                ", insuranceUnitPrice=" + insuranceUnitPrice +
                ", feeBOS=" + feeBOS +
                '}';
    }
}
