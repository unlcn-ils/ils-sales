package com.unlcn.ils.sales.backend.security;

import cn.huiyunche.commons.enums.SigninErrorEnum;
import cn.huiyunche.commons.utils.AppUtil;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 认证失败处理.
 *
 * @author qichao
 */
public class JwtAuthenticationFailureHandler implements AuthenticationFailureHandler {

    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        AppUtil.responseErrorJson(response, SigninErrorEnum.UsermamePasswordInvlidError);
    }

}
