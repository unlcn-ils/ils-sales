package com.unlcn.ils.sales.backend.service;

import com.unlcn.ils.sales.backend.bo.UserBankCardViewBO;
import com.unlcn.ils.sales.backend.bo.UserBankcardBO;

import java.util.List;
import java.util.Map;

/**
 * Created by houjianhui on 2017/6/8.
 */
public interface UserBankcardService {
    /**
     * 保存银行卡信息
     *
     * @param bo 银行卡信息
     * @return
     * @throws Exception
     */
    Integer save(UserBankcardBO bo) throws Exception;

    /**
     * 根据用户ID获取银行卡
     *
     * @return
     * @throws Exception
     */
    List<UserBankCardViewBO> listUserBankcard() throws Exception;

    /**
     * 校验银行卡类型
     *
     * @param card 银行卡号
     *
     * @return
     * @throws Exception
     */
    Map<String, String> checkBankCard(String card) throws Exception;

}
