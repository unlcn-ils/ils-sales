package com.unlcn.ils.sales.backend.redis;

import org.springframework.data.redis.connection.RedisSentinelConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisShardInfo;

/**
 * minds redis连接工厂.
 * <p>
 * <strong>一定要在finally中调用jedis关闭连接.</strong>
 * <p>
 * 用于连接的获取.
 * <p>
 * <p>
 * <pre>
 *
 * &#64;Service
 * public class TestRedisService {
 *
 * 	&#64;Autowired
 * 	private MindsJedisConnectionFactory connectionFactory = null;
 *
 * 	private void commence() {
 * 		Jedis jedis = this.getConnectionFactory().fetchJedisConnector();
 * 		try {
 * 			// do something
 *        } finally {
 * 			jedis.close();
 *        }
 *    }
 *
 * 	private MindsJedisConnectionFactory getConnectionFactory() {
 * 		return this.connectionFactory;
 *    }
 *
 * }
 * </pre>
 * <p>
 *
 * @author zhaoshb
 * @see
 * @since 0.0.1
 */
public class MindsJedisConnectionFactory extends JedisConnectionFactory {

    public MindsJedisConnectionFactory() {
        super();
    }

    public MindsJedisConnectionFactory(JedisPoolConfig poolConfig) {
        super(poolConfig);
    }

    public MindsJedisConnectionFactory(JedisShardInfo shardInfo) {
        super(shardInfo);
    }

    public MindsJedisConnectionFactory(RedisSentinelConfiguration sentinelConfig, JedisPoolConfig poolConfig) {
        super(sentinelConfig, poolConfig);
    }

    public MindsJedisConnectionFactory(RedisSentinelConfiguration sentinelConfig) {
        super(sentinelConfig);
    }

    @Override
    public Jedis fetchJedisConnector() {
        return super.fetchJedisConnector();
    }

}
