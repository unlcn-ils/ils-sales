package com.unlcn.ils.sales.backend.bo;

/**
 * Created by houjianhui on 2017/6/13.
 */
public class BankBO {
    private Integer id;
    private String code;
    private String name;
    private String logo;
    private String logoColor;
    private String backgroundLogo;
    private String logoSecond;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getLogoColor() {
        return logoColor;
    }

    public void setLogoColor(String logoColor) {
        this.logoColor = logoColor;
    }

    public String getBackgroundLogo() {
        return backgroundLogo;
    }

    public void setBackgroundLogo(String backgroundLogo) {
        this.backgroundLogo = backgroundLogo;
    }

    public String getLogoSecond() {
        return logoSecond;
    }

    public void setLogoSecond(String logoSecond) {
        this.logoSecond = logoSecond;
    }

    @Override
    public String toString() {
        return "BankBO{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", logo='" + logo + '\'' +
                ", logoColor='" + logoColor + '\'' +
                ", backgroundLogo='" + backgroundLogo + '\'' +
                ", logoSecond='" + logoSecond + '\'' +
                '}';
    }
}
