package com.unlcn.ils.sales.backend.enums;

/**
 * Created by houjianhui on 2017/6/10.
 */
public enum OrderStatusEnum {

    ORDER_INIT(10, "待审批"),
    ORDER_AWAIT(20, "待接单"),
    ORDER_TOUCH(30, "已接单"),
    ORDER_DELIVERY(40, "已交车"),
    ORDER_RECOVER(50, "已收款");

    private final int value;
    private final String text;

    OrderStatusEnum(int value, String text) {
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static OrderStatusEnum getByValue(int value){
        for (OrderStatusEnum temp : OrderStatusEnum.values()) {
            if(temp.getValue() == value){
                return temp;
            }
        }
        return null;
    }
}
