package com.unlcn.ils.sales.backend.enums;

/**
 * 易宝校验卡类型是否可用
 *
 * @author hdy [Tuffy]
 */
public enum YeepayCardValidEnum {
    NO("0", "不可用"),
    YES("1", "可用");

    private String value;
    private String text;

    YeepayCardValidEnum(String value, String text) {
        this.value = value;
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static YeepayCardValidEnum getByValue(String value) {
        for (YeepayCardValidEnum temp : YeepayCardValidEnum.values()) {
            if (temp.getValue().equals(value)) {
                return temp;
            }
        }
        return null;
    }
}
