package com.unlcn.ils.sales.backend.bo;

/**
 * Created by houjianhui on 2017/6/13.
 */
public class UserViewBO {
    private Integer id;
    private String loginName;
    private String phone;
    private Integer type;
    private Integer userStatus;
    private Boolean enable;
    private String realName;
    private String email;
    private String addr;
    private String picKey;
    private Integer saleCompanyId;
    private Integer saleCompanyDivisionId;
    private String picUrl;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getPicKey() {
        return picKey;
    }

    public void setPicKey(String picKey) {
        this.picKey = picKey;
    }

    public Integer getSaleCompanyId() {
        return saleCompanyId;
    }

    public void setSaleCompanyId(Integer saleCompanyId) {
        this.saleCompanyId = saleCompanyId;
    }

    public Integer getSaleCompanyDivisionId() {
        return saleCompanyDivisionId;
    }

    public void setSaleCompanyDivisionId(Integer saleCompanyDivisionId) {
        this.saleCompanyDivisionId = saleCompanyDivisionId;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    @Override
    public String toString() {
        return "UserViewBO{" +
                "id=" + id +
                ", loginName='" + loginName + '\'' +
                ", phone='" + phone + '\'' +
                ", type=" + type +
                ", userStatus=" + userStatus +
                ", enable=" + enable +
                ", realName='" + realName + '\'' +
                ", email='" + email + '\'' +
                ", addr='" + addr + '\'' +
                ", picKey='" + picKey + '\'' +
                ", saleCompanyId=" + saleCompanyId +
                ", saleCompanyDivisionId=" + saleCompanyDivisionId +
                ", picUrl='" + picUrl + '\'' +
                '}';
    }
}
