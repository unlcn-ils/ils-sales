package com.unlcn.ils.sales.backend.bo;

/**
 * Created by houjianhui on 2017/6/14.
 */
public class OrderToKyleBO {
    private Integer userId;
    private String orderCode;

    private String departRegionCode;
    private String departRegionName;
    private String departContact;
    private String departPhone;

    private String destRegionCode;
    private String destRegionName;
    private String destContact;
    private String destPhone;

    private String pickDate;
    private Integer brands;
    private Integer brandId;
    private String brandName;
    private String vehicleId;
    private String vehicleName;
    private Integer seriesAmt;

    private String distance;
    private String orderCost;
    private String bxCost;
    private Integer orderStatus;

    private String orderSku;

    private String comment;

    // 是否开票
    private Boolean isTicket;
    // 运输方式
    private Integer transportType;
    // 是否上门提车
    private Boolean isPick;
    // 是否送车上门
    private Boolean isDeliv;
    // 提车详细地址
    private String departAddr;
    // 送车详细地址
    private String destAddr;
    // 提车时间
    private String pickTime;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getDepartRegionCode() {
        return departRegionCode;
    }

    public void setDepartRegionCode(String departRegionCode) {
        this.departRegionCode = departRegionCode;
    }

    public String getDepartRegionName() {
        return departRegionName;
    }

    public void setDepartRegionName(String departRegionName) {
        this.departRegionName = departRegionName;
    }

    public String getDepartContact() {
        return departContact;
    }

    public void setDepartContact(String departContact) {
        this.departContact = departContact;
    }

    public String getDepartPhone() {
        return departPhone;
    }

    public void setDepartPhone(String departPhone) {
        this.departPhone = departPhone;
    }

    public String getDestRegionCode() {
        return destRegionCode;
    }

    public void setDestRegionCode(String destRegionCode) {
        this.destRegionCode = destRegionCode;
    }

    public String getDestRegionName() {
        return destRegionName;
    }

    public void setDestRegionName(String destRegionName) {
        this.destRegionName = destRegionName;
    }

    public String getDestContact() {
        return destContact;
    }

    public void setDestContact(String destContact) {
        this.destContact = destContact;
    }

    public String getDestPhone() {
        return destPhone;
    }

    public void setDestPhone(String destPhone) {
        this.destPhone = destPhone;
    }

    public String getPickDate() {
        return pickDate;
    }

    public void setPickDate(String pickDate) {
        this.pickDate = pickDate;
    }

    public Integer getBrands() {
        return brands;
    }

    public void setBrands(Integer brands) {
        this.brands = brands;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public Integer getSeriesAmt() {
        return seriesAmt;
    }

    public void setSeriesAmt(Integer seriesAmt) {
        this.seriesAmt = seriesAmt;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getOrderCost() {
        return orderCost;
    }

    public void setOrderCost(String orderCost) {
        this.orderCost = orderCost;
    }

    public String getBxCost() {
        return bxCost;
    }

    public void setBxCost(String bxCost) {
        this.bxCost = bxCost;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderSku() {
        return orderSku;
    }

    public void setOrderSku(String orderSku) {
        this.orderSku = orderSku;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    public Boolean getIsTicket() {
        return isTicket;
    }

    public void setIsTicket(Boolean isTicket) {
        this.isTicket = isTicket;
    }

    public Integer getTransportType() {
        return transportType;
    }

    public void setTransportType(Integer transportType) {
        this.transportType = transportType;
    }

    public Boolean getIsPick() {
        return isPick;
    }

    public void setIsPick(Boolean isPick) {
        this.isPick = isPick;
    }

    public Boolean getIsDeliv() {
        return isDeliv;
    }

    public void setIsDeliv(Boolean isDeliv) {
        this.isDeliv = isDeliv;
    }

    public String getDepartAddr() {
        return departAddr;
    }

    public void setDepartAddr(String departAddr) {
        this.departAddr = departAddr;
    }

    public String getDestAddr() {
        return destAddr;
    }

    public void setDestAddr(String destAddr) {
        this.destAddr = destAddr;
    }

    public String getPickTime() {
        return pickTime;
    }

    public void setPickTime(String pickTime) {
        this.pickTime = pickTime;
    }
    @Override
    public String toString() {
        return "OrderToKyleBO{" +
                "userId=" + userId +
                ", orderCode='" + orderCode + '\'' +
                ", departRegionCode='" + departRegionCode + '\'' +
                ", departRegionName='" + departRegionName + '\'' +
                ", departContact='" + departContact + '\'' +
                ", departPhone='" + departPhone + '\'' +
                ", destRegionCode='" + destRegionCode + '\'' +
                ", destRegionName='" + destRegionName + '\'' +
                ", destContact='" + destContact + '\'' +
                ", destPhone='" + destPhone + '\'' +
                ", pickDate='" + pickDate + '\'' +
                ", brands=" + brands +
                ", brandId=" + brandId +
                ", brandName='" + brandName + '\'' +
                ", vehicleId='" + vehicleId + '\'' +
                ", vehicleName='" + vehicleName + '\'' +
                ", seriesAmt=" + seriesAmt +
                ", distance='" + distance + '\'' +
                ", orderCost='" + orderCost + '\'' +
                ", bxCost='" + bxCost + '\'' +
                ", orderStatus=" + orderStatus +
                ", orderSku='" + orderSku + '\'' +
                ", comment='" + comment + '\'' +
                ", isTicket=" + isTicket +
                ", transportType=" + transportType +
                ", isPick=" + isPick +
                ", isDeliv=" + isDeliv +
                ", departAddr=" + departAddr +
                ", destAddr=" + destAddr +
                ", pickTime=" + pickTime +
                '}';
    }
}
