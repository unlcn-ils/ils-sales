package com.unlcn.ils.sales.backend.util;


import com.unlcn.ils.sales.backend.bo.IdSnowFlake;

/**
 * Created by houjianhui on 2017/5/31.
 */
public class UniquenessUtil {
    /**
     * @param prifix 前缀
     * @return
     * @Title: generateUniquenessFlag
     * @Description: 根据前缀生成唯一性标示
     * @return: String
     */
    public static String generateUniquenessFlag(String prifix) {
        IdSnowFlake id = new IdSnowFlake();
        return String.valueOf(prifix + id.getId());
    }
}
