package com.unlcn.ils.sales.backend.service;

import com.unlcn.ils.sales.backend.bo.BonusBillBO;

import java.util.Date;

/**
 * Created by houjianhui on 2017/6/7.
 */
public interface BonusBillService {

    /**
     * 保存账单
     *
     * @param bo 账单
     * @return
     * @throws Exception
     */

    Integer save(BonusBillBO bo) throws Exception;

    /**
     * 查询账单信息
     *
     * @param date 生成账单时间
     * @return
     * @throws Exception
     */
    BonusBillBO getBonusBill(Date date) throws Exception;
}
