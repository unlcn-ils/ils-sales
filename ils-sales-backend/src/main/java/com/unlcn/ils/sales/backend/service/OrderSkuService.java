package com.unlcn.ils.sales.backend.service;

import com.unlcn.ils.sales.backend.bo.BrandBO;

import java.util.List;

/**
 * Created by houjianhui on 2017/5/27.
 */
public interface OrderSkuService {
    /**
     * 保存销售订单商品车信息
     *
     * @param bo 销售订单商品车信息
     * @param saleOrderId 销售订单ID
     * @return
     * @throws Exception
     */
    Integer save(String bo, Integer saleOrderId) throws Exception;

    /**
     * 查询商品车信息
     *
     * @param cvKeyword
     * @return
     * @throws Exception
     */
    List<BrandBO> listBrand(String cvKeyword) throws Exception;
}
