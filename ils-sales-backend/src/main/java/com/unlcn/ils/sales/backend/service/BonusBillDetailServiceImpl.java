package com.unlcn.ils.sales.backend.service;

import cn.huiyunche.commons.domain.PageVo;
import cn.huiyunche.commons.exception.BusinessException;
import com.google.common.collect.Lists;
import com.unlcn.ils.sales.backend.bo.BonusBillDetailBO;
import com.unlcn.ils.sales.backend.bo.BonusBillDetailQueryBO;
import com.unlcn.ils.sales.base.mapper.SaleBonusBillDetailMapper;
import com.unlcn.ils.sales.base.mapper.SaleBonusBillDetailViewMapper;
import com.unlcn.ils.sales.base.model.BillDetailViewDAO;
import com.unlcn.ils.sales.base.model.SaleBonusBillDetail;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by houjianhui on 2017/6/8.
 */
@Service
public class BonusBillDetailServiceImpl implements BonusBillDetailService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BonusBillDetailServiceImpl.class);

    @Autowired
    private SaleBonusBillDetailMapper bonusBillDetailMapper;

    @Autowired
    private SaleBonusBillDetailViewMapper bonusBillDetailViewMapper;

    @Override
    public Integer save(BonusBillDetailBO bo) throws Exception {
        LOGGER.info("BonusBillDetailServiceImpl.save params BonusBillDetailBO: {}", bo);
        if (Objects.equals(bo, null) || Objects.equals(bo, "")) {
            LOGGER.info("BonusBillDetailServiceImpl.save params BonusBillDetailBO must not be null");
            throw new IllegalArgumentException("账单明细不能为空");
        }
        SaleBonusBillDetail detail = new SaleBonusBillDetail();
        BeanUtils.copyProperties(bo, detail);
        try {
            return bonusBillDetailMapper.insertSelective(detail);
        } catch (Exception e) {
            LOGGER.error("BonusBillDetailServiceImpl.save error: {}", e);
            throw new BusinessException("保存账单明细信息异常");
        }
    }

    @Override
    public Map<String, Object> listBonusBillDetail(PageVo pageVo, Integer billId) throws Exception {
        LOGGER.info("BonusBillDetailServiceImpl.listBonusBillDetail params PageVo: {}, billId: {}", pageVo, billId);
        if (Objects.equals(pageVo, null) || Objects.equals(pageVo, "")) {
            LOGGER.info("BonusBillDetailServiceImpl.listBonusBillDetail param PageVo must not be null");
            throw new IllegalArgumentException("分页参数不能为空");
        } else if (Objects.equals(billId, null) || Objects.equals(billId, "") || Objects.equals(billId, 0)) {
            LOGGER.info("BonusBillDetailServiceImpl.listBonusBillDetail param billId must not be null");
        }
        // 构建查询条件
        Map<String, Object> queryMap = new HashMap<>();
        queryMap.put("billId", billId);

        try {
            pageVo.setTotalRecord(bonusBillDetailViewMapper.countBillDetail(queryMap));
        } catch (Exception e) {
            LOGGER.error("BonusBillDetailServiceImpl.listBonusBillDetail TotalRecord error: {}", e);
            throw new BusinessException("统计账单明细异常");
        }

        // 返回值
        Map<String, Object> map = new HashMap<>();
        if (!Objects.equals(pageVo, null) && !Objects.equals(pageVo, "")) {
            queryMap.put("orderByClause", StringUtils.isNotBlank(pageVo.getOrder()) == true ? pageVo.getOrder() : "sbbd.gmt_create desc");
            queryMap.put("limitStart", pageVo.getStartIndex());
            queryMap.put("limitEnd", pageVo.getPageSize());
            map.put("page", pageVo);
        }

        List<BillDetailViewDAO> dtos = Lists.newArrayList();
        try {
            dtos = bonusBillDetailViewMapper.listBillDetail(queryMap);
        } catch (Exception e) {
            LOGGER.error("BonusBillDetailServiceImpl.listBonusBillDetail error: {}", e);
            throw new BusinessException("查询账单明细异常");
        }

        List<BonusBillDetailQueryBO> detailQueryBOS = Lists.newArrayList();

        if (CollectionUtils.isNotEmpty(dtos)) {
            dtos.stream().forEach(val -> {
                BonusBillDetailQueryBO bo = new BonusBillDetailQueryBO();
                BeanUtils.copyProperties(val, bo);
                detailQueryBOS.add(bo);
            });
        }
        map.put("billDetails", detailQueryBOS);

        return map;
    }
}