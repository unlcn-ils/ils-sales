package com.unlcn.ils.sales.backend.service;

import com.unlcn.ils.sales.backend.bo.BonusBalanceBO;

import java.math.BigDecimal;

/**
 * Created by houjianhui on 2017/6/5.
 */
public interface BonusBalanceService {

    /**
     * 根据用户ID查询用户余额
     *
     * @return
     * @throws Exception
     */
    BigDecimal getUserBalanceByUserId() throws Exception;

    /**
     * 根据用户ID更新用户钱包余额信息
     *
     * @param balance 用户钱包余额
     * @param withdrawalBalance 用户钱包可提现余额
     * @return
     * @throws Exception
     */
    Integer updateUserBalance(BigDecimal balance, BigDecimal withdrawalBalance) throws Exception;

    /**
     * 新增用户钱包余额信息
     *
     * @param bo 销售奖励余额BO
     * @return
     * @throws Exception
     */
    Integer save(BonusBalanceBO bo) throws Exception;
}
