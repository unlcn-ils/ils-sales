package com.unlcn.ils.sales.backend.bo;

/**
 * Created by houjianhui on 2017/6/9.
 */
public class UserBO {
    private Integer id;
    private String loginName;
    private String phone;
    private Integer type;
    private Integer userStatus;
    private String pwd;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    @Override
    public String toString() {
        return "UserBO{" +
                "id=" + id +
                ", loginName='" + loginName + '\'' +
                ", phone='" + phone + '\'' +
                ", type=" + type +
                ", userStatus=" + userStatus +
                ", pwd='" + pwd + '\'' +
                '}';
    }
}
