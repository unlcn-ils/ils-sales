package com.unlcn.ils.sales.backend.service;

import com.unlcn.ils.sales.backend.bo.OrderAuditBO;

/**
 * Created by houjianhui on 2017/6/12.
 */
public interface OrderAuditService {
    /**
     * 保存订单审核
     *
     * @param saleOrderId 销售订单ID
     * @param saleOrderCode 销售订单编码
     * @return
     * @throws Exception
     */
    Integer save(Integer saleOrderId, String saleOrderCode) throws Exception;

    /**
     * 更新销售订单审核信息
     *
     * @param bo
     * @return
     * @throws Exception
     */
    Integer update(OrderAuditBO bo) throws Exception;

    /**
     * 更新销售订单审核状态
     *
     * @param ids 销售订单审核ID
     * @return
     * @throws Exception
     */
    Integer updateAuditStatus(String ids) throws Exception;

    /**
     * 更新销售订单审核状态
     *
     * @param saleOrderId 销售订单ID
     * @param auditStatus 审核状态
     */
    void updateAuditStatusByOrderId(Integer saleOrderId, Integer auditStatus);
}
