package com.unlcn.ils.sales.backend.service;

import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.sales.backend.bo.BonusWithdrawLogBO;
import com.unlcn.ils.sales.base.mapper.SaleBonusWithdrawLogMapper;
import com.unlcn.ils.sales.base.model.SaleBonusWithdrawLog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * Created by houjianhui on 2017/6/8.
 */
@Service
public class BonusWithdrawLogServiceImpl implements BonusWithdrawLogService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BonusWithdrawLogServiceImpl.class);

    @Autowired
    private SaleBonusWithdrawLogMapper bonusWithdrawLogMapper;

    @Override
    public Integer save(BonusWithdrawLogBO bo) throws Exception {
        LOGGER.info("BonusWithdrawLogServiceImpl.save param BonusWithdrawLogBO: {}", bo);
        if (Objects.equals(bo, null) || Objects.equals(bo, "")) {
            LOGGER.info("BonusWithdrawLogServiceImpl.save param BonusWithdrawLogBO must not be null");
            throw new IllegalArgumentException("提现日志信息不能为空");
        }
        SaleBonusWithdrawLog log = new SaleBonusWithdrawLog();
        BeanUtils.copyProperties(bo, log);
        try {
            bonusWithdrawLogMapper.insertSelective(log);
        } catch (Exception e) {
            LOGGER.error("BonusWithdrawLogServiceImpl.save error: {}", e);
            throw new BusinessException("保存提现日志信息异常");
        }
        return null;
    }
}
