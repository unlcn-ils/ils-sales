package com.unlcn.ils.sales.backend.constant;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * Created by houjianhui on 2017/8/17.
 */
@Configuration
public class AutoConfConstant {

    @Value("${ils.sc.url}")
    private String sc;

    @Value("${ils.sc.url.timeout}")
    private Integer scTime;

    @Value("${ils.kyle.url}")
    private String kyle;

    @Value("${ils.kyle.url.timeout}")
    private Integer kyleTime;

    @Value("${ils.bms.url}")
    private String bms;

    @Value("${ils.bms.url.timeout}")
    private Integer bmsTime;

    @Value("${ils.sale.brands}")
    private Integer brands;

    @Value("${secure.key}")
    private String secure;

    @Value("${qiniuBankLogoBucket}")
    private String BankLogo;


    @Value("${secure.bank.key}")
    private String bankSecureKey;

    @Value("${hyc.operation.url}")
    private String operation;

    @Value("${hyc.operation.url.timeout}")
    private Integer operationTime;


    public String getSc() {
        return sc;
    }

    public void setSc(String sc) {
        this.sc = sc;
    }

    public Integer getScTime() {
        return scTime;
    }

    public void setScTime(Integer scTime) {
        this.scTime = scTime;
    }

    public String getKyle() {
        return kyle;
    }

    public void setKyle(String kyle) {
        this.kyle = kyle;
    }

    public Integer getKyleTime() {
        return kyleTime;
    }

    public void setKyleTime(Integer kyleTime) {
        this.kyleTime = kyleTime;
    }

    public String getBms() {
        return bms;
    }

    public void setBms(String bms) {
        this.bms = bms;
    }

    public Integer getBmsTime() {
        return bmsTime;
    }

    public void setBmsTime(Integer bmsTime) {
        this.bmsTime = bmsTime;
    }

    public Integer getBrands() {
        return brands;
    }

    public void setBrands(Integer brands) {
        this.brands = brands;
    }

    public String getSecure() {
        return secure;
    }

    public void setSecure(String secure) {
        this.secure = secure;
    }

    public String getBankLogo() {
        return BankLogo;
    }

    public void setBankLogo(String bankLogo) {
        BankLogo = bankLogo;
    }

    public String getBankSecureKey() {
        return bankSecureKey;
    }

    public void setBankSecureKey(String bankSecureKey) {
        this.bankSecureKey = bankSecureKey;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public Integer getOperationTime() {
        return operationTime;
    }

    public void setOperationTime(Integer operationTime) {
        this.operationTime = operationTime;
    }
}
