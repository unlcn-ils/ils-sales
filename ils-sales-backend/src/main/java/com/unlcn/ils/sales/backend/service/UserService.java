package com.unlcn.ils.sales.backend.service;

import com.unlcn.ils.sales.backend.bo.UserBO;
import com.unlcn.ils.sales.backend.bo.UserViewBO;

import java.util.List;
import java.util.Map;

/**
 * Created by houjianhui on 2017/6/9.
 */
public interface UserService {

    /**
     * 用户登录接口
     *
     * @param loginName 登录名称
     * @param pwd 密码
     * @return
     * @throws Exception
     */
    Map login(String loginName, String pwd) throws Exception;

    /**
     * 根据登录名称获取用户信息
     *
     * @param loginName 登录名称
     * @return
     * @throws Exception
     */
    UserBO getUserByLoginName(String loginName) throws Exception;

    /**
     * 设置验证码
     *
     * @param phone 手机号
     * @param code 验证码
     * @param seconds 时间
     * @return
     * @throws Exception
     */
    String setAuthCode(String phone, String code, int seconds) throws Exception;

    /**
     * 校验手机号是否存在
     *
     * @param phone 手机号
     * @return
     */
    boolean alreadyExists(String phone);

    /**
     * 发送验证码
     *
     * @param phone 手机号
     * @return
     * @throws Exception
     */
    String captcha(String phone) throws Exception;

    /**
     * 根据手机号获取用户信息
     *
     * @param phone 手机号
     * @return
     * @throws Exception
     */
    UserBO getUserByPhone(String phone) throws Exception;

    /**
     * 获取存储的验证码
     *
     * @param phone 手机号
     * @return
     * @throws Exception
     */
    String getAuthCode(String phone) throws Exception;

    /**
     * 移除redis中AuthCode
     *
     * @param key
     * @return
     * @throws Exception
     */
    Boolean removeAuthcode(String key) throws Exception;

    /**
     * 重置密码
     *
     * @param phone 手机号
     * @param pwd 密码
     * @param authcode 验证码
     * @return
     * @throws Exception
     */
    Integer restPwd(String phone, String pwd, String authcode) throws Exception;

    /**
     * 获取用户信息
     *
     * @param id 用户ID
     * @param loginName 登录名称
     * @param phone 手机号
     * @return
     * @throws Exception
     */
    UserViewBO getUserByCondition(Integer id, String loginName, String phone) throws Exception;

    /**
     * 获取当前登录用户信息
     *
     * @return
     * @throws Exception
     */
    UserViewBO getCurrentUser() throws Exception;

    /**
     * 查询销售人员列表
     *
     * @return
     * @throws Exception
     */
    List<UserBO> listUser() throws Exception;

}
