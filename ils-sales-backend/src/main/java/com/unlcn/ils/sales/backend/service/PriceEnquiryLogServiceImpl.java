package com.unlcn.ils.sales.backend.service;

import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.sales.backend.bo.PriceEnquiryLogBO;
import com.unlcn.ils.sales.base.mapper.SalePriceEnquiryLogMapper;
import com.unlcn.ils.sales.base.model.SalePriceEnquiryLog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * Created by houjianhui on 2017/6/5.
 */
@Service
public class PriceEnquiryLogServiceImpl implements PriceEnquiryLogService{

    private static final Logger LOGGER = LoggerFactory.getLogger(PriceEnquiryLogServiceImpl.class);

    @Autowired
    private SalePriceEnquiryLogMapper priceEnquiryLogMapper;

    @Override
    public Integer save(PriceEnquiryLogBO bo) throws Exception {
        LOGGER.info("PriceEnquiryLogServiceImpl.save param PriceEnquiryLogBO: {}", bo);
        if (Objects.equals(bo, null) || Objects.equals(bo, "")) {
            LOGGER.info("PriceEnquiryLogServiceImpl.save param PriceEnquiryLogBO must not be null");
            throw new IllegalArgumentException("销售奖励询价日志表单不能为空");
        }

        SalePriceEnquiryLog log = new SalePriceEnquiryLog();
        BeanUtils.copyProperties(bo, log);

        Integer res;
        try {
            res = priceEnquiryLogMapper.insertSelective(log);
        } catch (Exception e) {
            LOGGER.error("PriceEnquiryLogServiceImpl.save error: {}", e);
            throw new BusinessException("保存销售奖励询价日志异常");
        }
        return res;
    }
}
