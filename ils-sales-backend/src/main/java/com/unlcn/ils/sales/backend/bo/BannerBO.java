package com.unlcn.ils.sales.backend.bo;

/**
 * Created by houjianhui on 2017/6/10.
 */
public class BannerBO {

    private String clientType;
    private String appType;
    private String picKey;
    private String picHref;
    private String picUrl;

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public String getAppType() {
        return appType;
    }

    public void setAppType(String appType) {
        this.appType = appType;
    }

    public String getPicKey() {
        return picKey;
    }

    public void setPicKey(String picKey) {
        this.picKey = picKey;
    }

    public String getPicHref() {
        return picHref;
    }

    public void setPicHref(String picHref) {
        this.picHref = picHref;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    @Override
    public String toString() {
        return "BannerBO{" +
                "clientType='" + clientType + '\'' +
                ", appType='" + appType + '\'' +
                ", picKey='" + picKey + '\'' +
                ", picHref='" + picHref + '\'' +
                ", picUrl='" + picUrl + '\'' +
                '}';
    }
}
