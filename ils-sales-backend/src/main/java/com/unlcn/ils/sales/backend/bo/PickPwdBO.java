package com.unlcn.ils.sales.backend.bo;

/**
 * Created by houjianhui on 2017/6/8.
 */
public class PickPwdBO {

    private Integer userId;
    private String userName;
    private Integer errorAmt;
    private String pwd;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getErrorAmt() {
        return errorAmt;
    }

    public void setErrorAmt(Integer errorAmt) {
        this.errorAmt = errorAmt;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    @Override
    public String toString() {
        return "PickPwdBO{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                ", errorAmt=" + errorAmt +
                ", pwd='" + pwd + '\'' +
                '}';
    }
}
