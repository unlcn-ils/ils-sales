package com.unlcn.ils.sales.backend.enums;

/**
 * 银行卡类型
 *
 * @author hdy [Tuffy]
 */
public enum YeepayCardTypeEnum {
    DEBIT("1", "储蓄卡"),
    CREDIT("2", "信用卡"),
    UNKNOW("-1", "未知银行卡");

    private String value;
    private String text;

    YeepayCardTypeEnum(String value, String text) {
        this.value = value;
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static YeepayCardTypeEnum getByValue(String value) {
        for (YeepayCardTypeEnum temp : YeepayCardTypeEnum.values()) {
            if (temp.getValue().equals(value)) {
                return temp;
            }
        }
        return null;
    }
}
