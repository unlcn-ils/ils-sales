package com.unlcn.ils.sales.backend.redis;

import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * 供分布式session使用.
 *
 * @author zhaoshb
 * @since 0.0.1
 */
@EnableRedisHttpSession(maxInactiveIntervalInSeconds = 3600)
public class EmbeddedRedisConfiguration {

}
