package com.unlcn.ils.sales.backend.bo;

import java.math.BigDecimal;

/**
 * Created by houjianhui on 2017/6/9.
 */
public class PriceBO {
    private Integer lineId;
    private Integer programId;
    private BigDecimal mileage;
    private BigDecimal ltlUnitPrice;
    private BigDecimal ltlGrossPrice;
    private BigDecimal tlUnitPrice;
    private BigDecimal tlGrossPrice;
    private String transModeCode;
    private String transModeName;
    private Integer carryToolId;
    private String carryToolName;
    private BigDecimal unitPrice;

    public Integer getLineId() {
        return lineId;
    }

    public void setLineId(Integer lineId) {
        this.lineId = lineId;
    }

    public Integer getProgramId() {
        return programId;
    }

    public void setProgramId(Integer programId) {
        this.programId = programId;
    }

    public BigDecimal getMileage() {
        return mileage;
    }

    public void setMileage(BigDecimal mileage) {
        this.mileage = mileage;
    }

    public BigDecimal getLtlUnitPrice() {
        return ltlUnitPrice;
    }

    public void setLtlUnitPrice(BigDecimal ltlUnitPrice) {
        this.ltlUnitPrice = ltlUnitPrice;
    }

    public BigDecimal getLtlGrossPrice() {
        return ltlGrossPrice;
    }

    public void setLtlGrossPrice(BigDecimal ltlGrossPrice) {
        this.ltlGrossPrice = ltlGrossPrice;
    }

    public BigDecimal getTlUnitPrice() {
        return tlUnitPrice;
    }

    public void setTlUnitPrice(BigDecimal tlUnitPrice) {
        this.tlUnitPrice = tlUnitPrice;
    }

    public BigDecimal getTlGrossPrice() {
        return tlGrossPrice;
    }

    public void setTlGrossPrice(BigDecimal tlGrossPrice) {
        this.tlGrossPrice = tlGrossPrice;
    }

    public String getTransModeCode() {
        return transModeCode;
    }

    public void setTransModeCode(String transModeCode) {
        this.transModeCode = transModeCode;
    }

    public String getTransModeName() {
        return transModeName;
    }

    public void setTransModeName(String transModeName) {
        this.transModeName = transModeName;
    }

    public Integer getCarryToolId() {
        return carryToolId;
    }

    public void setCarryToolId(Integer carryToolId) {
        this.carryToolId = carryToolId;
    }

    public String getCarryToolName() {
        return carryToolName;
    }

    public void setCarryToolName(String carryToolName) {
        this.carryToolName = carryToolName;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    @Override
    public String toString() {
        return "PriceBO{" +
                "lineId=" + lineId +
                ", programId=" + programId +
                ", mileage=" + mileage +
                ", ltlUnitPrice=" + ltlUnitPrice +
                ", ltlGrossPrice=" + ltlGrossPrice +
                ", tlUnitPrice=" + tlUnitPrice +
                ", tlGrossPrice=" + tlGrossPrice +
                ", transModeCode='" + transModeCode + '\'' +
                ", transModeName='" + transModeName + '\'' +
                ", carryToolId=" + carryToolId +
                ", carryToolName='" + carryToolName + '\'' +
                ", unitPrice=" + unitPrice +
                '}';
    }
}
