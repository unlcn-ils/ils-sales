package com.unlcn.ils.sales.backend.bo;

import java.math.BigDecimal;

/**
 * Created by houjianhui on 2017/6/5.
 */
public class BonusBalanceBO {

    private Integer userId;
    private String userName;
    private BigDecimal balance;
    private BigDecimal withdrawalBalance;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getWithdrawalBalance() {
        return withdrawalBalance;
    }

    public void setWithdrawalBalance(BigDecimal withdrawalBalance) {
        this.withdrawalBalance = withdrawalBalance;
    }

    @Override
    public String toString() {
        return "BonusBalanceBO{" +
                ", userId=" + userId +
                ", userName='" + userName + '\'' +
                ", balance=" + balance +
                ", withdrawalBalance=" + withdrawalBalance +
                '}';
    }
}
