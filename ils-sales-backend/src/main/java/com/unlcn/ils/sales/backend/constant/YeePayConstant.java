package com.unlcn.ils.sales.backend.constant;

/**
 * 易宝常量类
 */
public class YeePayConstant {

    /**
     * 易宝代付接口地址key
     */
    public static final String YEEPAY_CASH_URL = "yeepayCashUrl";
    /**
     * 易宝代付商户id
     */
    public static final String YEEPAY_GROUP_ID = "yeepayGroupId";
    /**
     * 易宝代付证书
     */
    public static final String YEEPAY_GROUP_CA = "yeepayGroupCa";
    /**
     * 易宝key证书名
     */
    public static final String YEEPAY_KEY_PFX_NAME = "yeepayKeyPfxName";
    /**
     * 易宝Certp证书名
     */
    public static final String YEEPAY_CERT_PFX_NAME = "yeepayCertPfxName";
    /**
     * 易宝证书key密码
     */
    public static final String YEEPAY_KEY_PASSWORD = "yeepayKeyPassword";
    /**
     * 易宝证书密码
     */
    public static final String YEEPAY_CERTP_PASSWORD = "yeepayCertpPassword";
    /**
     * 易宝商户支付私钥
     */
    public static final String YEEPAY_PAY_PRIVATE_KEY = "yeepayPayPrivateKey";
    /**
     * 易宝商户支付公钥
     */
    public static final String YEEPAY_PAY_PUBLIC_KEY = "yeepayPayPublicKey";
    /**
     * 易宝商户支付卡密校验 id
     */
    public static final String YEEPAY_PAY_MERCHANTACCOUNT = "merchantaccount";
    /**
     * 易宝商户支付卡密校验 私钥
     */
    public static final String YEEPAY_PAY_MERCHANTACCOUNT_PRIVATE_KEY = "merchantPrivateKey";
    /**
     * 易宝商户支付卡密校验 公钥
     */
    public static final String YEEPAY_PAY_MERCHANTACCOUNT_PUBLIC_KEY = "merchantPublicKey";
    /**
     * 易宝提现日
     */
    public static final String YEEPAY_CASH_DAY = "yeepayCashDay";
    /**
     * 易宝网页支付商户
     */
    public static final String YEEPAY_WEBPAY_GROUPID = "yeepayWebPayGroupId";
    /**
     * 易宝网页支付证书
     */
    public static final String YEEPAY_WEBPAY_GROUPCA = "yeepayWebPayGroupCa";
}
