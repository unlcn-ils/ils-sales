package com.unlcn.ils.sales.backend.service;

import com.unlcn.ils.sales.backend.bo.AreaBO;
import com.unlcn.ils.sales.backend.bo.OrderBO;

import java.util.List;

/**
 * Created by houjianhui on 2017/5/27.
 */
public interface OrderAddrService {

    /**
     * 保存销售订单地址及联系人信息
     *
     * @param bo 销售订单BO
     * @param saleOrderId 销售订单ID
     * @return
     * @throws Exception
     */
    Integer save(OrderBO bo, Integer saleOrderId) throws Exception;

    /**
     * 查询起运地区域信息
     *
     * @param areaKeyword 输入参数
     * @return
     * @throws Exception
     */
    List<AreaBO> listAreaByCondition(String areaKeyword) throws Exception;

    /**
     * 查询目的地区域（省）信息
     *
     * @param
     * @return
     * @throws Exception
     */
    List<AreaBO> listAreaByCondition() throws Exception;

    /**
     * 根据起运地ID、目的地省ID查询区域信息
     *
     * @param endProvinceId 目的地省ID
     * @return
     */
    List<AreaBO> listAreaByCondition(Integer endProvinceId) throws Exception;
}
