package com.unlcn.ils.sales.backend.bo;

/**
 * Created by houjianhui on 2017/6/9.
 */
public class AreaBO {
    private Integer id;
    private String code;
    private String name;
    private String sname;
    private String parentCode;
    private String path;
    private String status;
    private String level;
    private String pinyin;
    private String initialWords;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getPinyin() {
        return pinyin;
    }

    public void setPinyin(String pinyin) {
        this.pinyin = pinyin;
    }

    public String getInitialWords() {
        return initialWords;
    }

    public void setInitialWords(String initialWords) {
        this.initialWords = initialWords;
    }

    @Override
    public String toString() {
        return "AreaBO{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", sname='" + sname + '\'' +
                ", parentCode='" + parentCode + '\'' +
                ", path='" + path + '\'' +
                ", status='" + status + '\'' +
                ", level='" + level + '\'' +
                ", pinyin='" + pinyin + '\'' +
                ", initialWords='" + initialWords + '\'' +
                '}';
    }
}
