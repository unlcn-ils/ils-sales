package com.unlcn.ils.sales.backend.bo;

import java.math.BigDecimal;

/**
 * Created by houjianhui on 2017/6/8.
 */
public class BonusWithdrawLogBO {
    private Integer userId;
    private String userName;
    private Integer saleBankcardId;
    private BigDecimal pickMoney;
    private Integer status;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getSaleBankcardId() {
        return saleBankcardId;
    }

    public void setSaleBankcardId(Integer saleBankcardId) {
        this.saleBankcardId = saleBankcardId;
    }

    public BigDecimal getPickMoney() {
        return pickMoney;
    }

    public void setPickMoney(BigDecimal pickMoney) {
        this.pickMoney = pickMoney;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "BonusWithdrawLogBO{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                ", saleBankcardId=" + saleBankcardId +
                ", pickMoney=" + pickMoney +
                ", status=" + status +
                '}';
    }
}
