package com.unlcn.ils.sales.backend.bo;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by houjianhui on 2017/5/26.
 */
public class OrderBO {

    // 销售订单表
    private Integer id;
    private String code;
    private Integer userId;
    @NotNull(message = "提车日期不能为空")
    private Date pickDate;

    @NotBlank(message = "提车时间不能为空")
    private String pickTime;
    @NotNull(message = "是否上门提车不能为空")
    private Integer isPick;
    @NotNull(message = "是否送车上门不能为空")
    private Integer isDeliv;
    @NotNull(message = "运输方式不能为空")
    private Integer transportType;
    @NotNull(message = "是否开票不能为空")
    private Integer isTicket;
    //"发票抬头"
    private String companyName;
    // "纳税人识别码"
    private String identifyNumber;

    @NotNull(message = "商品车品牌个数不能为空")
    private Integer brands;
    @NotNull(message = "商品车数量不能为空")
    private Integer seriesAmt;
    @NotNull(message = "销售订单标准售价不能为空")
    private BigDecimal standardSalePrice;
    @NotNull(message = "销售订单标准单公里运价不能为空")
    private BigDecimal standardUnitPrice;
    @NotNull(message = "销售订单销售人员建议价格不能为空")
    private BigDecimal suggestSalePrice;
    @NotNull(message = "商品车保险总价")
    private BigDecimal insuranceSalePrice;
    @NotNull(message = "商品车保险单价")
    private BigDecimal insuranceUnitPrice;
    private BigDecimal finalSalePrice;
    private BigDecimal finalPurchasePrice;
    private Integer orderStatus;
    private String comment;

    // 销售订单地址表
    @NotBlank(message = "发车地编码不能为空")
    private String departRegionCode;
    @NotBlank(message = "发车地名称不能为空")
    private String departRegionName;
    @NotBlank(message = "发车联系人不能为空")
    private String departContact;
    @NotBlank(message = "发车联系人电话不能为空")
    private String departPhone;
    @NotBlank(message = "目的地编码不能为空")
    private String destRegionCode;
    @NotBlank(message = "目的地名称不能为空")
    private String destRegionName;
    @NotBlank(message = "收车联系人不能为空")
    private String destContact;
    @NotBlank(message = "收车联系人电话不能为空")
    private String destPhone;
    @NotNull(message = "里程不能为空")
    private BigDecimal distance;
    @NotNull(message = "线路ID不能为空")
    private Integer lineId;

    // 销售订单商品车表
    @NotBlank(message = "商品车信息不能为空")
    private String orderSkuBOS;

    // 销售订单标签
    private String orderTagBOS;
    //  发车详细地址
    private String departAddr;
    //  收车详细地址
    private String destAddr;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getPickDate() {
        return pickDate;
    }

    public void setPickDate(Date pickDate) {
        this.pickDate = pickDate;
    }

    public Integer getBrands() {
        return brands;
    }

    public void setBrands(Integer brands) {
        this.brands = brands;
    }

    public Integer getSeriesAmt() {
        return seriesAmt;
    }

    public void setSeriesAmt(Integer seriesAmt) {
        this.seriesAmt = seriesAmt;
    }

    public BigDecimal getStandardSalePrice() {
        return standardSalePrice;
    }

    public void setStandardSalePrice(BigDecimal standardSalePrice) {
        this.standardSalePrice = standardSalePrice;
    }

    public BigDecimal getStandardUnitPrice() {
        return standardUnitPrice;
    }

    public void setStandardUnitPrice(BigDecimal standardUnitPrice) {
        this.standardUnitPrice = standardUnitPrice;
    }

    public BigDecimal getSuggestSalePrice() {
        return suggestSalePrice;
    }

    public void setSuggestSalePrice(BigDecimal suggestSalePrice) {
        this.suggestSalePrice = suggestSalePrice;
    }

    public BigDecimal getFinalSalePrice() {
        return finalSalePrice;
    }

    public void setFinalSalePrice(BigDecimal finalSalePrice) {
        this.finalSalePrice = finalSalePrice;
    }

    public BigDecimal getFinalPurchasePrice() {
        return finalPurchasePrice;
    }

    public void setFinalPurchasePrice(BigDecimal finalPurchasePrice) {
        this.finalPurchasePrice = finalPurchasePrice;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDepartRegionCode() {
        return departRegionCode;
    }

    public void setDepartRegionCode(String departRegionCode) {
        this.departRegionCode = departRegionCode;
    }

    public String getDepartRegionName() {
        return departRegionName;
    }

    public void setDepartRegionName(String departRegionName) {
        this.departRegionName = departRegionName;
    }

    public String getDepartContact() {
        return departContact;
    }

    public void setDepartContact(String departContact) {
        this.departContact = departContact;
    }

    public String getDepartPhone() {
        return departPhone;
    }

    public void setDepartPhone(String departPhone) {
        this.departPhone = departPhone;
    }

    public String getDestRegionCode() {
        return destRegionCode;
    }

    public void setDestRegionCode(String destRegionCode) {
        this.destRegionCode = destRegionCode;
    }

    public String getDestRegionName() {
        return destRegionName;
    }

    public void setDestRegionName(String destRegionName) {
        this.destRegionName = destRegionName;
    }

    public String getDestContact() {
        return destContact;
    }

    public void setDestContact(String destContact) {
        this.destContact = destContact;
    }

    public String getDestPhone() {
        return destPhone;
    }

    public void setDestPhone(String destPhone) {
        this.destPhone = destPhone;
    }

    public BigDecimal getDistance() {
        return distance;
    }

    public void setDistance(BigDecimal distance) {
        this.distance = distance;
    }

    public String getOrderSkuBOS() {
        return orderSkuBOS;
    }

    public void setOrderSkuBOS(String orderSkuBOS) {
        this.orderSkuBOS = orderSkuBOS;
    }

    public String getOrderTagBOS() {
        return orderTagBOS;
    }

    public void setOrderTagBOS(String orderTagBOS) {
        this.orderTagBOS = orderTagBOS;
    }

    public Integer getLineId() {
        return lineId;
    }

    public void setLineId(Integer lineId) {
        this.lineId = lineId;
    }

    public BigDecimal getInsuranceSalePrice() {
        return insuranceSalePrice;
    }

    public void setInsuranceSalePrice(BigDecimal insuranceSalePrice) {
        this.insuranceSalePrice = insuranceSalePrice;
    }

    public BigDecimal getInsuranceUnitPrice() {
        return insuranceUnitPrice;
    }

    public void setInsuranceUnitPrice(BigDecimal insuranceUnitPrice) {
        this.insuranceUnitPrice = insuranceUnitPrice;
    }

    public String getPickTime() {
        return pickTime;
    }

    public void setPickTime(String pickTime) {
        this.pickTime = pickTime;
    }

    public Integer getIsPick() {
        return isPick;
    }

    public void setIsPick(Integer isPick) {
        this.isPick = isPick;
    }

    public Integer getIsDeliv() {
        return isDeliv;
    }

    public void setIsDeliv(Integer isDeliv) {
        this.isDeliv = isDeliv;
    }

    public Integer getTransportType() {
        return transportType;
    }

    public void setTransportType(Integer transportType) {
        this.transportType = transportType;
    }

    public Integer getIsTicket() {
        return isTicket;
    }

    public void setIsTicket(Integer isTicket) {
        this.isTicket = isTicket;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getIdentifyNumber() {
        return identifyNumber;
    }

    public void setIdentifyNumber(String identifyNumber) {
        this.identifyNumber = identifyNumber;
    }

    public String getDepartAddr() {
        return departAddr;
    }

    public void setDepartAddr(String departAddr) {
        this.departAddr = departAddr;
    }

    public String getDestAddr() {
        return destAddr;
    }

    public void setDestAddr(String destAddr) {
        this.destAddr = destAddr;
    }

    @Override
    public String toString() {
        return "OrderBO{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", userId=" + userId +
                ", pickDate=" + pickDate +
                ", pickTime='" + pickTime + '\'' +
                ", isPick='" + isPick + '\'' +
                ", isDeliv='" + isDeliv + '\'' +
                ", transportType='" + transportType + '\'' +
                ", isTicket='" + isTicket + '\'' +
                ", companyName='" + companyName + '\'' +
                ", identifyNumber='" + identifyNumber + '\'' +
                ", brands=" + brands +
                ", seriesAmt=" + seriesAmt +
                ", standardSalePrice=" + standardSalePrice +
                ", standardUnitPrice=" + standardUnitPrice +
                ", suggestSalePrice=" + suggestSalePrice +
                ", insuranceSalePrice=" + insuranceSalePrice +
                ", insuranceUnitPrice=" + insuranceUnitPrice +
                ", finalSalePrice=" + finalSalePrice +
                ", finalPurchasePrice=" + finalPurchasePrice +
                ", orderStatus=" + orderStatus +
                ", comment='" + comment + '\'' +
                ", departRegionCode='" + departRegionCode + '\'' +
                ", departRegionName='" + departRegionName + '\'' +
                ", departContact='" + departContact + '\'' +
                ", departPhone='" + departPhone + '\'' +
                ", destRegionCode='" + destRegionCode + '\'' +
                ", destRegionName='" + destRegionName + '\'' +
                ", destContact='" + destContact + '\'' +
                ", destPhone='" + destPhone + '\'' +
                ", distance=" + distance +
                ", lineId=" + lineId +
                ", orderSkuBOS='" + orderSkuBOS + '\'' +
                ", orderTagBOS='" + orderTagBOS + '\'' +
                ", departAddr='" + departAddr + '\'' +
                ", destAddr='" + destAddr + '\'' +
                '}';
    }
}
