package com.unlcn.ils.sales.backend.service;

import com.unlcn.ils.sales.backend.bo.PickPwdBO;

/**
 * Created by houjianhui on 2017/6/8.
 */
public interface PickPwdService {

    /**
     * 查询当前用户是否设置提现密码
     *
     * @return
     * @throws Exception
     */
    Boolean isSettingPwd() throws Exception;

    /**
     * 获取输入错误密码次数
     *
     * @return
     * @throws Exception
     */
    Integer getErrorAmtByUserId() throws Exception;

    /**
     * 更新输入错误密码次数
     *
     * @param errorAmt
     * @return
     * @throws Exception
     */
    Integer updateErrorAmt(Integer errorAmt) throws Exception;

    /**
     * 保存提现密码
     *
     * @param bo 提现密码信息
     * @return
     * @throws Exception
     */
    Integer save(PickPwdBO bo) throws Exception;

    /**
     * 校验提现密码
     *
     * @param pwd 密码
     * @return
     * @throws Exception
     */
    Boolean verificationPickPwd(String pwd) throws Exception;

    /**
     * 更新提现密码
     *
     * @param pwd 密码
     * @return
     * @throws Exception
     */
    Integer updatePickPwd(String pwd) throws Exception;
}
