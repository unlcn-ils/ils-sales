package com.unlcn.ils.sales.backend.util;

import cn.huiyunche.commons.exception.BusinessException;
import com.alibaba.fastjson.JSONObject;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Author ：houjianhui
 * @Date : 2017/5/26.
 */
@ControllerAdvice
public class ExceptionHandlerController {

    @ExceptionHandler(value = {BusinessException.class})
    @ResponseBody
    public JSONObject businessExceptionHandler(BusinessException e) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("success", e.isSuccess());
        jsonObject.put("messageCode", e.getMessageCode());
        jsonObject.put("message", e.getLocalizedMessage());

        return jsonObject;
    }

    @ExceptionHandler(value = {Exception.class})
    @ResponseBody
    public JSONObject defaultExceptionHandler(Exception e) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("success", false);
        jsonObject.put("messageCode", "-1");
        jsonObject.put("message", e.getLocalizedMessage());
        return jsonObject;
    }
}
