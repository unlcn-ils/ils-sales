package com.unlcn.ils.sales.backend.bo;

import java.math.BigDecimal;

/**
 * Created by houjianhui on 2017/6/8.
 */
public class BonusBillDetailBO {
    private Integer carAmount;//商品车车辆数
    private BigDecimal saleOrderBonus;//销售订单奖金
    private Integer saleBonusBillId;//账单ID
    private Integer saleOrderId;//销售订单ID
    private String saleOrderCode;//销售订单编码

    public Integer getCarAmount() {
        return carAmount;
    }

    public void setCarAmount(Integer carAmount) {
        this.carAmount = carAmount;
    }

    public BigDecimal getSaleOrderBonus() {
        return saleOrderBonus;
    }

    public void setSaleOrderBonus(BigDecimal saleOrderBonus) {
        this.saleOrderBonus = saleOrderBonus;
    }

    public Integer getSaleBonusBillId() {
        return saleBonusBillId;
    }

    public void setSaleBonusBillId(Integer saleBonusBillId) {
        this.saleBonusBillId = saleBonusBillId;
    }

    public Integer getSaleOrderId() {
        return saleOrderId;
    }

    public void setSaleOrderId(Integer saleOrderId) {
        this.saleOrderId = saleOrderId;
    }

    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    @Override
    public String toString() {
        return "BonusBillDetailBO{" +
                "carAmount=" + carAmount +
                ", saleOrderBonus=" + saleOrderBonus +
                ", saleBonusBillId=" + saleBonusBillId +
                ", saleOrderId=" + saleOrderId +
                ", saleOrderCode='" + saleOrderCode + '\'' +
                '}';
    }
}
