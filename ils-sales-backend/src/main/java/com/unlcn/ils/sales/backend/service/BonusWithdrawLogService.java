package com.unlcn.ils.sales.backend.service;

import com.unlcn.ils.sales.backend.bo.BonusWithdrawLogBO;

/**
 * Created by houjianhui on 2017/6/8.
 */
public interface BonusWithdrawLogService {

    /**
     * 保存提现日志
     *
     * @param bo 提现日志信息
     * @return
     * @throws Exception
     */
    Integer save(BonusWithdrawLogBO bo) throws Exception;
}
