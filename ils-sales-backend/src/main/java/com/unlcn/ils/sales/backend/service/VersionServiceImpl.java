package com.unlcn.ils.sales.backend.service;

import com.unlcn.ils.sales.backend.enums.VersionEnum;
import com.unlcn.ils.sales.base.mapper.SaleVersionNoteMapper;
import com.unlcn.ils.sales.base.mapper.SaleVersionNoteViewMapper;
import com.unlcn.ils.sales.base.model.SaleVersionNote;
import com.unlcn.ils.sales.base.model.SaleVersionNoteExample;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * Created by houjianhui on 2017/9/18.
 */
@Service
public class VersionServiceImpl implements VersionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(VersionServiceImpl.class);

    @Autowired
    private SaleVersionNoteViewMapper saleVersionNoteViewMapper;

    @Autowired
    private SaleVersionNoteMapper saleVersionNoteMapper;

    @Override
    public SaleVersionNote getLasterVersionIOS(String platform) {
        return getVersionNote(platform, VersionEnum.IOS.getValue());
    }

    @Override
    public SaleVersionNote getLasterVersionAndroid(String platform) {
        return getVersionNote(platform, VersionEnum.ANDROID.getValue());
    }

    private SaleVersionNote getVersionNote(String project, Integer appType) {
        return saleVersionNoteViewMapper.selectLatestVersionNote(project, appType);
    }

    @Override
    public List<SaleVersionNote> list() throws Exception {
        return saleVersionNoteMapper.selectByExample(new SaleVersionNoteExample());
    }

    @Override
    public int modifyVersion(SaleVersionNote versionNote) throws Exception {
        LOGGER.info("modifyVersion params versionNote: {}", versionNote);
        if (StringUtils.isBlank(versionNote.getAppType())) {
            LOGGER.info("appType is null");
            throw new IllegalArgumentException("APP类型不能为空");
        } else if (StringUtils.isBlank(versionNote.getPlatform())) {
            LOGGER.info("platform is null");
            throw new IllegalArgumentException("平台类型不能为空");
        } else if (StringUtils.isBlank(versionNote.getVersion())) {
            LOGGER.info("version is null");
            throw new IllegalArgumentException("版本号不能为空");
        } else if (versionNote.getVersionCode() == null) {
            LOGGER.info("versionCode is null");
            throw new IllegalArgumentException("版本发布序号不能为空");
        }
        if (null == versionNote.getId()) {
            return saleVersionNoteMapper.insertSelective(versionNote);
        } else {
            return saleVersionNoteMapper.updateByPrimaryKeySelective(versionNote);
        }
    }

    @Override
    public int deleteVersion(Long id) throws Exception {
        LOGGER.info("deleteVersion params id: {}", id);
        if (Objects.equals(id, 0) || Objects.equals(id, null)) {
            LOGGER.info("id is null");
            throw new IllegalArgumentException("版本ID不能为空");
        }
        return saleVersionNoteMapper.deleteByPrimaryKey(id);
    }

    @Override
    public SaleVersionNote selectVersionId(Long id) throws Exception {
        LOGGER.info("selectVersionId params id: {}", id);
        return saleVersionNoteMapper.selectByPrimaryKey(id);
    }
}
