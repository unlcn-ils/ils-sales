package com.unlcn.ils.sales.backend.bo;

/**
 * Created by houjianhui on 2017/5/27.
 */
public class OrderTagBO {
    private Integer saleOrderId;
    private String code;
    private String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSaleOrderId() {
        return saleOrderId;
    }

    public void setSaleOrderId(Integer saleOrderId) {
        this.saleOrderId = saleOrderId;
    }

    @Override
    public String toString() {
        return "OrderTagBO{" +
                "saleOrderId=" + saleOrderId +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
