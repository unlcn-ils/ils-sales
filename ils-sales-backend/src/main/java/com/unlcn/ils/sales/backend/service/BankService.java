package com.unlcn.ils.sales.backend.service;

import com.unlcn.ils.sales.backend.bo.BankBO;

/**
 * Created by houjianhui on 2017/6/13.
 */
public interface BankService {

    /**
     * 查询银行信息
     *
     * @param code 银行编码
     * @return
     * @throws Exception
     */
    BankBO getBankByCode(String code) throws Exception;
}
