package com.unlcn.ils.sales.backend.service;

import com.unlcn.ils.sales.backend.bo.BannerBO;

import java.util.List;

/**
 * Created by houjianhui on 2017/6/10.
 */
public interface BannerService {

    /**
     * 查询轮播图
     *
     * @param clientType 客户端类型
     * @param appType 应用类型
     * @param w 宽度
     * @param h 高度
     * @return
     * @throws Exception
     */
    List<BannerBO> listBanner(String clientType, String appType, String w, String h) throws Exception;
}
