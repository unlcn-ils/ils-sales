package com.unlcn.ils.sales.backend.bo;

/**
 * Created by houjianhui on 2017/6/12.
 */
public class OrderAuditBO {
    private Integer id;
    private Integer saleOrderId;
    private String saleOrderCode;
    private Integer auditStatus;
    private String comment;
    private Integer auditUser;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSaleOrderId() {
        return saleOrderId;
    }

    public void setSaleOrderId(Integer saleOrderId) {
        this.saleOrderId = saleOrderId;
    }

    public String getSaleOrderCode() {
        return saleOrderCode;
    }

    public void setSaleOrderCode(String saleOrderCode) {
        this.saleOrderCode = saleOrderCode;
    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getAuditUser() {
        return auditUser;
    }

    public void setAuditUser(Integer auditUser) {
        this.auditUser = auditUser;
    }

    @Override
    public String toString() {
        return "OrderAuditBO{" +
                "id=" + id +
                ", saleOrderId=" + saleOrderId +
                ", saleOrderCode='" + saleOrderCode + '\'' +
                ", auditStatus=" + auditStatus +
                ", comment='" + comment + '\'' +
                ", auditUser=" + auditUser +
                '}';
    }
}
