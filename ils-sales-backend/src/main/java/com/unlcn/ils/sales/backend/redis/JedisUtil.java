package com.unlcn.ils.sales.backend.redis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;

/**
 * Jedis 工具类
 * Created by LYN on 28/10/2016.
 */
@Component
public class JedisUtil {

    private static final Logger logger = LoggerFactory.getLogger(JedisUtil.class);

    private static MindsJedisConnectionFactory connectionFactory;

    /**
     * 注入缓存
     *
     * @param mindsJedisConnectionFactory 缓存工厂
     */
    @Autowired
    public void setConnectionFactory(MindsJedisConnectionFactory mindsJedisConnectionFactory) {
        connectionFactory = mindsJedisConnectionFactory;
    }

    /**
     * 获取数据
     *
     * @param key
     * @return
     */
    public static String get(String key) {
        Jedis jedis = connectionFactory.fetchJedisConnector();
        String value = null;
        try {
            value = jedis.get(key);
        } catch (Exception e) {
            //释放redis对象
            jedis.close();
        } finally {
            //返还到连接池
            jedis.close();
        }
        return value;
    }

    /**
     * 存储REDIS队列 顺序存储
     *
     * @param key   reids键名
     * @param value 键值
     */
    public static void push(byte[] key, byte[] value) {
        Jedis jedis = connectionFactory.fetchJedisConnector();
        try {
            jedis.rpush(key, value);
        } catch (Exception e) {
            jedis.close();
        } finally {
            jedis.close();
        }
    }


    /**
     * 获取队列数据
     *
     * @param key 键名
     * @return
     */
    public static byte[] pop(byte[] key) {
        byte[] bytes = null;
        Jedis jedis = connectionFactory.fetchJedisConnector();
        try {
            bytes = jedis.lpop(key);
        } catch (Exception e) {
            //释放redis对象
            jedis.close();
        } finally {
            //返还到连接池
            jedis.close();
        }
        return bytes;
    }

    /**
     * 获取队列长度
     *
     * @param key 键名
     * @return
     */
    public static Long llen(byte[] key) {
        Jedis jedis = connectionFactory.fetchJedisConnector();
        try {
            return jedis.llen(key);
        } catch (Exception e) {
            //释放redis对象
            jedis.close();
        } finally {
            //返还到连接池
            jedis.close();
        }
        return 0L;
    }

    /**
     * 删除 key
     *
     * @param key
     */
    public static void del(byte[] key) {
        Jedis jedis = connectionFactory.fetchJedisConnector();
        try {
            jedis.del(key);
        } catch (Exception e) {
            //释放redis对象
            jedis.close();
        } finally {
            jedis.close();
        }
    }

    /**
     * key 是否存在
     *
     * @param key reids键名
     */
    public static boolean exist(byte[] key) {
        Jedis jedis = connectionFactory.fetchJedisConnector();
        try {
            return jedis.exists(key);
        } catch (Exception e) {
            jedis.close();
        } finally {
            jedis.close();
        }
        return false;
    }

}
