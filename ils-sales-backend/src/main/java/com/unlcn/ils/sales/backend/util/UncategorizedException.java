package com.unlcn.ils.sales.backend.util;

/**
 * Created by houjianhui on 2017/6/13.
 */
public class UncategorizedException extends RuntimeException{
    private static final long serialVersionUID = 6417641452178955756L;

    public UncategorizedException() {
    }

    public UncategorizedException(String message) {
        super(message);
    }

    public UncategorizedException(Throwable cause) {
        super(cause);
    }

    public UncategorizedException(String message, Throwable cause) {
        super(message, cause);
    }
}
