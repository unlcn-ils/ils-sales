package com.unlcn.ils.sales.backend.enums;

/**
 * Created by houjianhui on 2017/6/10.
 */
public enum OrderAuditStatusEnum {

    ORDER_INIT(10, "待审批"),
    ORDER_AWAIT(20, "已通过"),
    ORDER_TOUCH(30, "未通过");

    private final int value;
    private final String text;

    OrderAuditStatusEnum(int value, String text) {
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static OrderAuditStatusEnum getByValue(int value){
        for (OrderAuditStatusEnum temp : OrderAuditStatusEnum.values()) {
            if(temp.getValue() == value){
                return temp;
            }
        }
        return null;
    }
}
