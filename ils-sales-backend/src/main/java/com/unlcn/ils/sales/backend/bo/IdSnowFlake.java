package com.unlcn.ils.sales.backend.bo;

import java.util.Random;

/**
 * Created by houjianhui on 2017/5/31.
 */
public class IdSnowFlake {
    private final long workerId;
    private final long datacenterId;
    private final long idepoch;
    private long sequence;
    private static final long datacenterIdShift = 17L;
    private static final long workerIdShift = 12L;
    private static final long timestampLeftShift = 22L;
    private static final long maxWorkerId = 31L;
    private static final long maxDatacenterId = 31L;
    private static final long sequenceMask = 4095L;
    private long lastTimestamp;
    private long defaultTimestamp;
    private static final Random r = new Random();

    public IdSnowFlake() {
        this(System.currentTimeMillis());
    }

    public IdSnowFlake(long idepoch) {
        this((long)r.nextInt(31), (long)r.nextInt(31), 0L, idepoch);
    }

    public IdSnowFlake(long workerId, long datacenterId) {
        this(workerId, datacenterId, 0L, System.currentTimeMillis());
    }

    public IdSnowFlake(long workerId, long datacenterId, long sequence) {
        this(workerId, datacenterId, sequence, System.currentTimeMillis());
    }

    public IdSnowFlake(long workerId, long datacenterId, long sequence, long idepoch) {
        this.lastTimestamp = -1L;
        this.defaultTimestamp = 1288834974657L;
        this.workerId = workerId;
        this.datacenterId = datacenterId;
        this.sequence = sequence;
        this.idepoch = idepoch;
        if(workerId >= 0L && workerId <= 31L) {
            if(datacenterId < 0L || datacenterId > 31L) {
                throw new IllegalArgumentException(String.format("非法datacenterId 节点(5位)应大于0而小于%d，而当前值为:%d ", new Object[]{Long.valueOf(31L), Long.valueOf(datacenterId)}));
            }
        } else {
            throw new IllegalArgumentException(String.format("非法workerId 数据中心(5位)应大于0而小于%d，而当前值为:%d", new Object[]{Long.valueOf(31L), Long.valueOf(workerId)}));
        }
    }

    public long getDatacenterId() {
        return this.datacenterId;
    }

    public long getIdepoch() {
        return this.idepoch;
    }

    public long getlastTimeStamp() {
        return this.lastTimestamp;
    }

    public long getSequence() {
        return this.sequence;
    }

    public long getWorkerId() {
        return this.workerId;
    }

    public long getTime() {
        return System.currentTimeMillis();
    }

    public long getId() {
        long id = this.nextId();
        return id;
    }

    public void setLastTimestamp(long lastTimestamp) {
        this.lastTimestamp = lastTimestamp;
    }

    private synchronized long nextId() {
        long timestamp = this.timeGen();
        if(timestamp < this.lastTimestamp) {
            throw new IllegalStateException(String.format("时间早于最低时间:%d", new Object[]{Long.valueOf(this.lastTimestamp)}));
        } else {
            if(this.lastTimestamp == timestamp) {
                this.sequence = this.sequence + 1L & 4095L;
                if(this.sequence == 0L) {
                    timestamp = this.tilNextMillis(this.lastTimestamp);
                }
            } else {
                this.sequence = 0L;
            }

            this.lastTimestamp = timestamp;
            long id = timestamp - this.defaultTimestamp << 22 | this.datacenterId << 17 | this.workerId << 12 | this.sequence;
            return id;
        }
    }

    private long tilNextMillis(long lastTimestamp) {
        long timestamp;
        for(timestamp = this.timeGen(); timestamp <= lastTimestamp; timestamp = this.timeGen()) {
            ;
        }

        return timestamp;
    }

    private long timeGen() {
        return System.currentTimeMillis();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("IdWorker{");
        sb.append("workerId=").append(this.workerId);
        sb.append(", datacenterId=").append(this.datacenterId);
        sb.append(", idepoch=").append(this.idepoch);
        sb.append(", lastTimestamp=").append(this.lastTimestamp);
        sb.append(", sequence=").append(this.sequence);
        sb.append('}');
        return sb.toString();
    }
}
