package com.unlcn.ils.sales.backend.service;

import com.unlcn.ils.sales.backend.bo.BankBO;
import com.unlcn.ils.sales.base.mapper.SaleBankMapper;
import com.unlcn.ils.sales.base.model.SaleBank;
import com.unlcn.ils.sales.base.model.SaleBankExample;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by houjianhui on 2017/6/13.
 */

@Service
public class BankServiceImpl implements BankService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BankServiceImpl.class);

    @Autowired
    private SaleBankMapper bankMapper;

    @Override
    public BankBO getBankByCode(String code) throws Exception {
        LOGGER.info("BankServiceImpl.getBankByCode param code: {}", code);
        if (StringUtils.isBlank(code)) {
            LOGGER.info("BankServiceImpl.getBankByCode param code must not be null");
            throw new IllegalArgumentException("银行编号不能为空");
        }
        SaleBankExample example = new SaleBankExample();
        example.createCriteria().andCodeEqualTo(code);
        List<SaleBank> banks = bankMapper.selectByExample(example);
        if (CollectionUtils.isNotEmpty(banks)) {
            BankBO bo = new BankBO();
            BeanUtils.copyProperties(banks.get(0), bo);
            return bo;
        }
        return null;
    }
}
