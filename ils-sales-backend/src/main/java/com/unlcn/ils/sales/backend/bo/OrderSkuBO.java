package com.unlcn.ils.sales.backend.bo;

import java.util.List;

/**
 * Created by houjianhui on 2017/5/27.
 */
public class OrderSkuBO {
    private Integer saleOrderId;
    private Integer brandId;
    private String brandCode;
    private String brandName;
    private Integer seriesId;
    private String seriesCode;
    private String seriesName;
    private Integer modelId;
    private String modelCode;
    private String modelName;
    private Integer amt;
    private List<String> vinList;

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public String getBrandCode() {
        return brandCode;
    }

    public void setBrandCode(String brandCode) {
        this.brandCode = brandCode;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public Integer getSeriesId() {
        return seriesId;
    }

    public void setSeriesId(Integer seriesId) {
        this.seriesId = seriesId;
    }

    public String getSeriesCode() {
        return seriesCode;
    }

    public void setSeriesCode(String seriesCode) {
        this.seriesCode = seriesCode;
    }

    public String getSeriesName() {
        return seriesName;
    }

    public void setSeriesName(String seriesName) {
        this.seriesName = seriesName;
    }

    public Integer getModelId() {
        return modelId;
    }

    public void setModelId(Integer modelId) {
        this.modelId = modelId;
    }

    public String getModelCode() {
        return modelCode;
    }

    public void setModelCode(String modelCode) {
        this.modelCode = modelCode;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public List<String> getVinList() {
        return vinList;
    }

    public void setVinList(List<String> vinList) {
        this.vinList = vinList;
    }

    public Integer getSaleOrderId() {
        return saleOrderId;
    }

    public void setSaleOrderId(Integer saleOrderId) {
        this.saleOrderId = saleOrderId;
    }

    public Integer getAmt() {
        return amt;
    }

    public void setAmt(Integer amt) {
        this.amt = amt;
    }

    @Override
    public String toString() {
        return "OrderSkuBO{" +
                "saleOrderId=" + saleOrderId +
                ", brandId=" + brandId +
                ", brandCode='" + brandCode + '\'' +
                ", brandName='" + brandName + '\'' +
                ", seriesId=" + seriesId +
                ", seriesCode='" + seriesCode + '\'' +
                ", seriesName='" + seriesName + '\'' +
                ", modelId=" + modelId +
                ", modelCode='" + modelCode + '\'' +
                ", modelName='" + modelName + '\'' +
                ", amt=" + amt +
                ", vinList='" + vinList + '\'' +
                '}';
    }
}
