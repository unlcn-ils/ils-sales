package com.unlcn.ils.sales.backend.bo;

/**
 * Created by houjianhui on 2017/6/5.
 */
public class PriceEnquiryLogBO {
    private Integer origin;
    private Integer dest;
    private Integer enquiryUser;

    public Integer getOrigin() {
        return origin;
    }

    public void setOrigin(Integer origin) {
        this.origin = origin;
    }

    public Integer getDest() {
        return dest;
    }

    public void setDest(Integer dest) {
        this.dest = dest;
    }

    public Integer getEnquiryUser() {
        return enquiryUser;
    }

    public void setEnquiryUser(Integer enquiryUser) {
        this.enquiryUser = enquiryUser;
    }

    @Override
    public String toString() {
        return "PriceEnquiryLogBO{" +
                "origin=" + origin +
                ", dest=" + dest +
                ", enquiryUser=" + enquiryUser +
                '}';
    }
}
