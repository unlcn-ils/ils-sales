package com.unlcn.ils.sales.backend.service;

/**
 * Created by houjianhui on 2017/5/27.
 */
public interface OrderTagService {

    /**
     * 保存销售订单标签信息
     *
     * @param bo 销售订单标签信息
     * @param saleOrderId 销售订单ID
     * @return
     * @throws Exception
     */
    Integer save(String bo, Integer saleOrderId) throws Exception;
}
