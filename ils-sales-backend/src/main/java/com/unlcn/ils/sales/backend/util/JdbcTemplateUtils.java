package com.unlcn.ils.sales.backend.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * jdbc工具类
 *
 * @author hdy [Tuffy]
 */
@Component
public class JdbcTemplateUtils {

    /**
     * jdbcTemplate对象
     */
    private static JdbcTemplate jdbc = null;

    /**
     * 注入缓存
     */
    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        jdbc = jdbcTemplate;
    }

    /**
     * 获取jdbc对象
     *
     * @return
     */
    public static JdbcTemplate getJdbcTemplate() {
        return jdbc;
    }
}
