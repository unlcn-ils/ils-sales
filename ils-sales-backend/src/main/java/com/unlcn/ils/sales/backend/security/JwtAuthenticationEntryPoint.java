package com.unlcn.ils.sales.backend.security;


import cn.huiyunche.commons.enums.SigninErrorEnum;
import cn.huiyunche.commons.utils.AppUtil;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 当用户没经过认证或者认证失败时会调用comence方法.
 * <p>
 * 返回对应的错误的json.
 *
 * @author qichao
 */
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {

    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        AppUtil.responseErrorJson(response, SigninErrorEnum.AuthenticationInvalidError);
    }

}
