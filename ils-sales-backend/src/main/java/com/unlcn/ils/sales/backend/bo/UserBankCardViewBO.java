package com.unlcn.ils.sales.backend.bo;

/**
 * 输出用户绑定卡信息
 *
 */
public class UserBankCardViewBO {

    private Integer id;
    private String card;
    private String bankName;
    private String bankLogo;
    private String bankWhiteLogo;
    private String bankLogoBg;
    private String bankLogoColor;

    public UserBankCardViewBO() {
    }

    public UserBankCardViewBO(Integer cardId, String card, String bankName, String bankLogo, String bankWhiteLogo, String bankLogoBg, String bankLogoColor) {
        this.id = cardId;
        this.card = card;
        this.bankName = bankName;
        this.bankLogo = bankLogo;
        this.bankWhiteLogo = bankWhiteLogo;
        this.bankLogoBg = bankLogoBg;
        this.bankLogoColor = bankLogoColor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer cardId) {
        this.id = cardId;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankLogo() {
        return bankLogo;
    }

    public void setBankLogo(String bankLogo) {
        this.bankLogo = bankLogo;
    }

    public String getBankWhiteLogo() {
        return bankWhiteLogo;
    }

    public void setBankWhiteLogo(String bankWhiteLogo) {
        this.bankWhiteLogo = bankWhiteLogo;
    }

    public String getBankLogoBg() {
        return bankLogoBg;
    }

    public void setBankLogoBg(String bankLogoBg) {
        this.bankLogoBg = bankLogoBg;
    }

    public String getBankLogoColor() {
        return bankLogoColor;
    }

    public void setBankLogoColor(String bankLogoColor) {
        this.bankLogoColor = bankLogoColor;
    }

}
