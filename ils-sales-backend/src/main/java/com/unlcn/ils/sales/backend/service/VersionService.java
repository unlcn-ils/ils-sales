package com.unlcn.ils.sales.backend.service;

import com.unlcn.ils.sales.base.model.SaleVersionNote;

import java.util.List;

/**
 * Created by houjianhui on 2017/9/18.
 */
public interface VersionService {
    /**
     * 获取IOS最新版本
     *
     * @return
     */
    SaleVersionNote getLasterVersionIOS(String platform);

    /**
     * 获取Android最新版本
     *
     * @return
     */
    SaleVersionNote getLasterVersionAndroid(String platform);

    /**
     * 查询版本列表
     *
     * @return
     */
    List<SaleVersionNote> list() throws Exception;

    /**
     * 数据修改
     *
     * @param versionNote
     * @return
     */
    int modifyVersion(SaleVersionNote versionNote) throws Exception;

    /**
     * 删除版本号
     *
     * @param id 版本ID
     * @return
     * @throws Exception
     */
    int deleteVersion(Long id) throws Exception;

    /**
     * 根据ID查询版本信息
     *
     * @param id
     * @return
     * @throws Exception
     */
    SaleVersionNote selectVersionId(Long id) throws Exception;
}
