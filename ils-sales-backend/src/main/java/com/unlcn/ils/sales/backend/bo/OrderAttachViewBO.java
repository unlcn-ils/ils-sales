package com.unlcn.ils.sales.backend.bo;

/**
 * Created by houjianhui on 2017/8/15.
 */
public class OrderAttachViewBO {
    private Integer id;
    private String picKey;
    private Integer saleOrderId;
    private String url;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPicKey() {
        return picKey;
    }

    public void setPicKey(String picKey) {
        this.picKey = picKey;
    }

    public Integer getSaleOrderId() {
        return saleOrderId;
    }

    public void setSaleOrderId(Integer saleOrderId) {
        this.saleOrderId = saleOrderId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "OrderAttachViewBO{" +
                "id=" + id +
                ", picKey='" + picKey + '\'' +
                ", saleOrderId=" + saleOrderId +
                ", url='" + url + '\'' +
                '}';
    }
}
