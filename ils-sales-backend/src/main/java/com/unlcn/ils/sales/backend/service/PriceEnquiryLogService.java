package com.unlcn.ils.sales.backend.service;

import com.unlcn.ils.sales.backend.bo.PriceEnquiryLogBO;

/**
 * Created by houjianhui on 2017/6/5.
 */
public interface PriceEnquiryLogService {

    /**
     * 保存销售奖励-询价日志
     *
     * @param bo 询价日志BO
     * @return
     * @throws Exception
     */
    Integer save(PriceEnquiryLogBO bo) throws Exception;
}
