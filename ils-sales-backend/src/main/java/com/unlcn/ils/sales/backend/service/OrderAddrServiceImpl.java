package com.unlcn.ils.sales.backend.service;

import cn.huiyunche.commons.exception.BusinessException;
import cn.huiyunche.commons.utils.HttpRequestUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.unlcn.ils.sales.backend.bo.AreaBO;
import com.unlcn.ils.sales.backend.bo.OrderBO;
import com.unlcn.ils.sales.backend.constant.AutoConfConstant;
import com.unlcn.ils.sales.base.mapper.SaleOrderAddrMapper;
import com.unlcn.ils.sales.base.model.SaleOrderAddr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * Created by houjianhui on 2017/5/27.
 */
@Service
public class OrderAddrServiceImpl implements OrderAddrService {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderAddrServiceImpl.class);

    @Autowired
    private SaleOrderAddrMapper orderAddrMapper;

    @Autowired
    private AutoConfConstant autoConfConstant;

    private static final String startArea = "startArea", endArea = "endArea", province = "province";

    @Override
    public Integer save(OrderBO bo, Integer saleOrderId) throws Exception {
        LOGGER.info("OrderAddrServiceImpl.save param: {}", bo);
        if (Objects.equals(bo, null) || Objects.equals(bo, "")) {
            LOGGER.info("OrderAddrServiceImpl.save param bo is null");
            throw new IllegalArgumentException("销售订单地址信息不能为空");
        } else if (Objects.equals(saleOrderId, null) || Objects.equals(saleOrderId, "")) {
            LOGGER.info("OrderAddrServiceImpl.save param saleOrderId is null");
            throw new IllegalArgumentException("销售订单ID不能为空");
        }

        // 校验数据
        verification(bo);

        // 保存数据
        SaleOrderAddr addr = new SaleOrderAddr();
        BeanUtils.copyProperties(bo, addr);
        addr.setSaleOrderId(saleOrderId);
        try {
           return orderAddrMapper.insertSelective(addr);
        } catch (Exception e) {
            LOGGER.error("OrderAddrServiceImpl.save error: {}", e);
            throw new BusinessException("保存销售订单地址信息异常");
        }
    }

    @Override
    public List<AreaBO> listAreaByCondition(String areaKeyword) throws Exception {
        return resolvAreaFormSCResult(null, null, null, startArea);
    }

    @Override
    public List<AreaBO> listAreaByCondition() throws Exception {
        return resolvAreaFormSCResult(null, null, null, province);
    }

    @Override
    public List<AreaBO> listAreaByCondition(Integer endProvinceId) throws Exception {
        return resolvAreaFormSCResult(null, null, endProvinceId, endArea);
    }

    private List<AreaBO> resolvAreaFormSCResult(String key, Integer startAreaId, Integer endProvinceId, String tag) throws Exception {
//        String url = autoConfConstant.getSc();
//        Integer time = autoConfConstant.getScTime();
        String url = autoConfConstant.getOperation();
        Integer time = autoConfConstant.getOperationTime();
        if (startArea.equals(tag)) {
//            url = url + "/api/network/lnlines/queryStartArea/" + key;
            url = url + "/api/line/queryStartArea/" + key;
        } else if (province.equals(tag)) {
//            url = url + "/api/network/lnlines/queryEndProvince/" + startAreaId ;
            url = url + "/api/area/getAllProvince";
        } else if (endArea.equals(tag)) {
//            url = url + "/api/network/lnlines/queryEndArea/" + startAreaId + "/" + endProvinceId;
            url = url + "/api/area/queryCityByPid/" + endProvinceId;
        }
        String result = HttpRequestUtil.sendHttpGet(url, null, time);
        if (!Objects.equals(result, null)) {
            JSONObject jsonObject = JSONObject.parseObject(result);
            String msgCode = jsonObject.getString("messageCode");
            String msg = jsonObject.getString("message");
            String data = jsonObject.getString("data");
            Boolean success = jsonObject.getBoolean("success");
            List<AreaBO> bos = JSONArray.parseArray(data, AreaBO.class);
            if (!success) {
                LOGGER.info("OrderAddr resolvAreaFormSCResult result error");
                throw new BusinessException(msgCode, msg);
            }
            return bos;
        }
        return null;
    }

    private void verification(OrderBO bo) throws Exception {
        if (Objects.equals(bo.getDepartRegionCode(), null) || Objects.equals(bo.getDepartRegionCode(), "")) {
            LOGGER.info("OrderAddrServiceImpl.save param DepartRegionCode must not be null");
            throw new IllegalArgumentException("起运地编码不能为空");
        } else if (Objects.equals(bo.getDepartRegionName(), null) || Objects.equals(bo.getDepartRegionName(), "")) {
            LOGGER.info("OrderAddrServiceImpl.save param DepartRegionName must not be null");
            throw new IllegalArgumentException("起运地名称不能为空");
//        } else if (Objects.equals(bo.getDepartContact(), null) || Objects.equals(bo.getDepartContact(), "")) {
//            LOGGER.info("OrderAddrServiceImpl.save param DepartContact must not be null");
//            throw new IllegalArgumentException("发车联系人不能为空");
//        } else if (Objects.equals(bo.getDepartPhone(), null) || Objects.equals(bo.getDepartPhone(), "")) {
//            LOGGER.info("OrderAddrServiceImpl.save param DepartPhone must not be null");
//            throw new IllegalArgumentException("发车人联系电话不能为空");
        } else if (Objects.equals(bo.getDestRegionCode(), null) || Objects.equals(bo.getDestRegionCode(), "")) {
            LOGGER.info("OrderAddrServiceImpl.save param DestRegionCode must not be null");
            throw new IllegalArgumentException("目的地编码不能为空");
        } else if (Objects.equals(bo.getDestRegionName(), null) || Objects.equals(bo.getDestRegionName(), "")) {
            LOGGER.info("OrderAddrServiceImpl.save param DestRegionName must not be null");
            throw new IllegalArgumentException("目的地名称不能为空");
//        } else if (Objects.equals(bo.getDestContact(), null) || Objects.equals(bo.getDestContact(), "")) {
//            LOGGER.info("OrderAddrServiceImpl.save param DestContact must not be null");
//            throw new IllegalArgumentException("收车联系人不能为空");
//        } else if (Objects.equals(bo.getDestPhone(), null) || Objects.equals(bo.getDestPhone(), "")) {
//            LOGGER.info("OrderAddrServiceImpl.save param DestPhone must not be null");
//            throw new IllegalArgumentException("收车联系人电话不能为空");
        } else if (Objects.equals(bo.getDistance(), null) || Objects.equals(bo.getDistance(), "")) {
            LOGGER.info("OrderAddrServiceImpl.save param Distance must not be null");
            throw new IllegalArgumentException("里程不能为空");
        } else if (Objects.equals(bo.getLineId(), null) || Objects.equals(bo.getLineId(), "")) {
            LOGGER.info("OrderAddrServiceImpl.save param LineId must not be null");
            throw new IllegalArgumentException("线路ID不能为空");
        }
    }
}
