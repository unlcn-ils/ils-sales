package com.unlcn.ils.sales.backend.service;

import cn.huiyunche.commons.domain.PageVo;
import com.unlcn.ils.sales.backend.bo.*;
import org.springframework.validation.BindingResult;

import java.util.Map;

/**
 * Created by houjianhui on 2017/5/26.
 */
public interface OrderService {

    /**
     * 保存销售订单信息
     *
     * @param bo 销售订单信息
     * @param br 校验结果
     * @return
     * @throws Exception
     */
    int save(OrderBO bo, BindingResult br) throws Exception;

    /**
     * 更新销售订单物流状态
     *
     * @param orderCode   销售订单编号
     * @param orderStatus 销售订单状态
     * @return
     * @throws Exception
     */
    Integer updateOrderStatusByOrderCode(String orderCode, Integer orderStatus) throws Exception;

    /**
     * 更新销售订单物流状态及支付状态
     *
     * @param orderCode   销售订单编号
     * @param orderStatus 销售订单状态
     * @param payStatus   销售订单支付状态
     * @return
     * @throws Exception
     */
    Integer updateOrderStatusAndPayStatusByOrderCode(String orderCode, Integer orderStatus, Integer payStatus) throws Exception;

    /**
     * 查询销售订单列表
     *
     * @param pageVo  分页对象
     * @param queryBO 查询订单条件
     * @return
     * @throws Exception
     */
    Map<String, Object> listSaleOrder(PageVo pageVo, OrderQueryBO queryBO) throws Exception;

    /**
     * 根据销售订单ID查询销售订单信息
     *
     * @param orderId 销售订单ID
     * @return
     * @throws Exception
     */
    OrderViewBO getSaleOrderByOrderId(Integer orderId) throws Exception;

    /**
     * 查询订单价格
     *
     * @param departRegionId 启运地ID
     * @param destRegionId   目的地ID
     * @param seriesAmt      商品车数量
     * @return
     * @throws Exception
     */
    OrderPriceBO getOrderPrice(Integer departRegionId, Integer destRegionId, Integer seriesAmt) throws Exception;

    /**
     * 运价查询
     *
     * @param departRegionId 起运地ID
     * @param destRegionId   目的地ID
     * @return
     * @throws Exception
     */
    FreightQueryBO getOrderPrice(Integer departRegionId, Integer destRegionId) throws Exception;

    /**
     * 销售订单列表
     *
     * @param pageVo  分页条件
     * @param queryBO 查询条件
     * @return
     * @throws Exception
     */
    Map<String, Object> listSaleOrderToPc(PageVo pageVo, OrderQueryBO queryBO) throws Exception;

    /**
     * 获取商户端订单状态更新销售端订单状态
     *
     * @return
     * @throws Exception
     */
    Integer updateSaleOrderStatusByKyleOrderStatus() throws Exception;

    /**
     * 更新销售订单附件审核状态
     *
     * @param orderCode 订单编号
     * @param attachAuditStatus 状态
     * @return
     * @throws Exception
     */
    Integer updateAttachAuditStatusBySaleOrderCode(String orderCode, Integer attachAuditStatus) throws Exception;

    /**
     * 更新销售订单附件审核状态
     *
     * @param id 销售订单ID
     * @param attachAuditStatus 状态
     * @return
     * @throws Exception
     */
    Integer updateAttachAuditStatusBySaleOrderId(Integer id, Integer attachAuditStatus) throws Exception;

}
