package com.unlcn.ils.sales.backend.util;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import java.util.List;
import java.util.Objects;

/**
 * Created by houjianhui on 2017/5/31.
 */
public class BoVerificationUtil {

    /**
     * 校验BO数据
     *
     * @param br 校验结果
     * @param LOGGER 日志
     * @throws IllegalArgumentException
     */
    public static void verification(BindingResult br, Logger LOGGER) throws IllegalArgumentException {
        if (Objects.equals(br, null) || Objects.equals(br, "")) {
            LOGGER.info("BindingResult must be not null");
            throw new IllegalArgumentException("校验对象不能为空");
        }
        if (br.hasErrors()) {
            List<ObjectError> errors = br.getAllErrors();
            if (CollectionUtils.isNotEmpty(errors)) {
                String msg = errors.get(0).getDefaultMessage();
                throw new IllegalArgumentException(msg);
            }
        }
    }

}
