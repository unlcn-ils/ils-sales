package com.unlcn.ils.sales.backend.enums;

/**
 * Created by houjianhui on 2017/6/10.
 */
public enum OrderPayStatusEnum {

    ORDER_INIT(10, "未支付"),
    ORDER_AWAIT(20, "待支付"),
    ORDER_TOUCH(30, "已支付");

    private final int value;
    private final String text;

    OrderPayStatusEnum(int value, String text) {
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static OrderPayStatusEnum getByValue(int value){
        for (OrderPayStatusEnum temp : OrderPayStatusEnum.values()) {
            if(temp.getValue() == value){
                return temp;
            }
        }
        return null;
    }
}
