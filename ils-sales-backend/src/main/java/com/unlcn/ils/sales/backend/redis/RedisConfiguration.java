package com.unlcn.ils.sales.backend.redis;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisSentinelConfiguration;
import org.springframework.util.StringUtils;

import java.util.Set;

/**
 * Redis哨兵配置.
 * <p>
 *
 * @author zhaoshb
 * @since 0.0.1
 */
@Configuration
public class RedisConfiguration {

    @Value("${spring.redis.sentinel.master}")
    private String sentinelMasterName = null;

    @Value("${spring.redis.sentinel.nodes}")
    private String hostAndPorts = null;

    @Bean
    public MindsJedisConnectionFactory connectionFactory() {
        Set<String> sentinelHostAndPorts = StringUtils.commaDelimitedListToSet(this.getHostAndPorts());
        RedisSentinelConfiguration sc = new RedisSentinelConfiguration(this.getSentinelMasterName(), sentinelHostAndPorts);

        return new MindsJedisConnectionFactory(sc);
    }

    private String getSentinelMasterName() {
        return this.sentinelMasterName;
    }

    private String getHostAndPorts() {
        return this.hostAndPorts;
    }

}
