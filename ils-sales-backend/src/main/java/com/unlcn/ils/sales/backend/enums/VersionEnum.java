package com.unlcn.ils.sales.backend.enums;

/**
 * 版本控制枚举
 */
public enum VersionEnum {

    ANDROID(10, "ANDROID"),
    IOS(20, "IOS");

    private final int value;
    private final String text;

    VersionEnum(int value, String text) {
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static VersionEnum getByValue(int value) {
        for (VersionEnum temp : VersionEnum.values()) {
            if (temp.getValue() == value) {
                return temp;
            }
        }
        return null;
    }
}
