package com.unlcn.ils.sales.backend.util;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.Validate;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.SecureRandom;

/**
 * Created by houjianhui on 2017/6/13.
 */
public class Encodes {
    private static final String DEFAULT_URL_ENCODING = "UTF-8";
    private static final char[] BASE62 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".toCharArray();

    public Encodes() {
    }

    public static String encodeHex(byte[] input) {
        return Hex.encodeHexString(input);
    }

    public static byte[] decodeHex(String input) {
        try {
            return Hex.decodeHex(input.toCharArray());
        } catch (DecoderException var2) {
            throw new UncategorizedException(var2);
        }
    }

    public static String encodeBase64(byte[] input) {
        return Base64.encodeBase64String(input);
    }

    public static String encodeUrlSafeBase64(byte[] input) {
        return Base64.encodeBase64URLSafeString(input);
    }

    public static byte[] decodeBase64(String input) {
        return Base64.decodeBase64(input);
    }

    public static String encodeBase62(byte[] input) {
        char[] chars = new char[input.length];

        for(int i = 0; i < input.length; ++i) {
            chars[i] = BASE62[(input[i] & 255) % BASE62.length];
        }

        return new String(chars);
    }

    public static String escapeHtml(String html) {
        return StringEscapeUtils.escapeHtml4(html);
    }

    public static String unescapeHtml(String htmlEscaped) {
        return StringEscapeUtils.unescapeHtml4(htmlEscaped);
    }

    public static String escapeXml(String xml) {
        return StringEscapeUtils.escapeXml(xml);
    }

    public static String unescapeXml(String xmlEscaped) {
        return StringEscapeUtils.unescapeXml(xmlEscaped);
    }

    public static String urlEncode(String part) {
        try {
            return URLEncoder.encode(part, "UTF-8");
        } catch (UnsupportedEncodingException var2) {
            throw new UncategorizedException(var2);
        }
    }

    public static String urlDecode(String part) {
        try {
            return URLDecoder.decode(part, "UTF-8");
        } catch (UnsupportedEncodingException var2) {
            throw new UncategorizedException(var2);
        }
    }

    public static String encryptAES(String content, String salt) {
        Preconditions.checkArgument(!Strings.isNullOrEmpty(content), "不能为空");

        try {
            KeyGenerator e = KeyGenerator.getInstance("AES");
            SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
            random.setSeed(salt.getBytes());
            e.init(128, random);
            SecretKey secretKey = e.generateKey();
            byte[] enCodeFormat = secretKey.getEncoded();
            SecretKeySpec key = new SecretKeySpec(enCodeFormat, "AES");
            Cipher cipher = Cipher.getInstance("AES");
            byte[] byteContent = content.getBytes("utf-8");
            cipher.init(1, key);
            byte[] result = cipher.doFinal(byteContent);
            StringBuffer sb = new StringBuffer();

            for(int i = 0; i < result.length; ++i) {
                String hex = Integer.toHexString(result[i] & 255);
                if(hex.length() == 1) {
                    hex = '0' + hex;
                }

                sb.append(hex.toUpperCase());
            }

            return sb.toString();
        } catch (Exception var13) {
            throw new UncategorizedException(var13);
        }
    }

    public static String decryptAES(String str, String salt) {
        Preconditions.checkArgument(!Strings.isNullOrEmpty(str), "不能为空");
        byte[] content = new byte[str.length() / 2];

        for(int e = 0; e < str.length() / 2; ++e) {
            int random = Integer.parseInt(str.substring(e * 2, e * 2 + 1), 16);
            int secretKey = Integer.parseInt(str.substring(e * 2 + 1, e * 2 + 2), 16);
            content[e] = (byte)(random * 16 + secretKey);
        }

        try {
            KeyGenerator var11 = KeyGenerator.getInstance("AES");
            SecureRandom var12 = SecureRandom.getInstance("SHA1PRNG");
            var12.setSeed(salt.getBytes());
            var11.init(128, var12);
            SecretKey var13 = var11.generateKey();
            byte[] enCodeFormat = var13.getEncoded();
            SecretKeySpec key = new SecretKeySpec(enCodeFormat, "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(2, key);
            byte[] result = cipher.doFinal(content);
            return new String(result);
        } catch (Exception var10) {
            throw new UncategorizedException(var10);
        }
    }

    public static String encodeBase64AES(String content, String salt) {
        Validate.notBlank(content, "待加密内容不能为空！", new Object[0]);
        byte[] bytes = content.getBytes();
        String result = encodeBase64(bytes);
        return encryptAES(result, salt);
    }

    public static String decodeBase64AES(String content, String salt) {
        Validate.notBlank(content, "待解密内容不能为空！", new Object[0]);
        String result = decryptAES(content, salt);
        byte[] bytes = decodeBase64(result);

        try {
            String strReturn = new String(bytes, "UTF-8");
            return strReturn;
        } catch (Exception var6) {
            throw new UncategorizedException(var6);
        }
    }
}
