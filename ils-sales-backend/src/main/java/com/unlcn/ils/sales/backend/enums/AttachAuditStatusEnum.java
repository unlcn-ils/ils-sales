package com.unlcn.ils.sales.backend.enums;

/**
 * Created by houjianhui on 2017/6/10.
 */
public enum AttachAuditStatusEnum {

    ORDER_INIT(10, "未提交"),
    ORDER_SUBMIT(20, "已提交"),
    ORDER_PASS(30, "已通过"),
    ORDER_REFUSE(40, "未通过");

    private final int value;
    private final String text;

    AttachAuditStatusEnum(int value, String text) {
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static AttachAuditStatusEnum getByValue(int value){
        for (AttachAuditStatusEnum temp : AttachAuditStatusEnum.values()) {
            if(temp.getValue() == value){
                return temp;
            }
        }
        return null;
    }
}
