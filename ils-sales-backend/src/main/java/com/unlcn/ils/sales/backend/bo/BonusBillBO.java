package com.unlcn.ils.sales.backend.bo;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by houjianhui on 2017/6/7.
 */
public class BonusBillBO {
    private Integer id;
    private Integer userId;
    private String userName;
    private BigDecimal saleOrderMoney;
    private Integer saleOrderAmount;
    private Integer carAmount;
    private BigDecimal saleOrderBonus;
    private Integer status;
    private Integer payStatus;
    private Date billBeginDate;
    private Date billEndDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public BigDecimal getSaleOrderMoney() {
        return saleOrderMoney;
    }

    public void setSaleOrderMoney(BigDecimal saleOrderMoney) {
        this.saleOrderMoney = saleOrderMoney;
    }

    public Integer getSaleOrderAmount() {
        return saleOrderAmount;
    }

    public void setSaleOrderAmount(Integer saleOrderAmount) {
        this.saleOrderAmount = saleOrderAmount;
    }

    public Integer getCarAmount() {
        return carAmount;
    }

    public void setCarAmount(Integer carAmount) {
        this.carAmount = carAmount;
    }

    public BigDecimal getSaleOrderBonus() {
        return saleOrderBonus;
    }

    public void setSaleOrderBonus(BigDecimal saleOrderBonus) {
        this.saleOrderBonus = saleOrderBonus;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }

    public Date getBillBeginDate() {
        return billBeginDate;
    }

    public void setBillBeginDate(Date billBeginDate) {
        this.billBeginDate = billBeginDate;
    }

    public Date getBillEndDate() {
        return billEndDate;
    }

    public void setBillEndDate(Date billEndDate) {
        this.billEndDate = billEndDate;
    }

    @Override
    public String toString() {
        return "BonusBillBO{" +
                "id=" + id +
                ", userId=" + userId +
                ", userName='" + userName + '\'' +
                ", saleOrderMoney=" + saleOrderMoney +
                ", saleOrderAmount=" + saleOrderAmount +
                ", carAmount=" + carAmount +
                ", saleOrderBonus=" + saleOrderBonus +
                ", status=" + status +
                ", payStatus=" + payStatus +
                ", billBeginDate=" + billBeginDate +
                ", billEndDate=" + billEndDate +
                '}';
    }
}
