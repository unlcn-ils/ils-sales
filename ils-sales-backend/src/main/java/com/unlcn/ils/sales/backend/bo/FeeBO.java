package com.unlcn.ils.sales.backend.bo;

/**
 * Created by houjianhui on 2017/6/9.
 */
public class FeeBO {
    private String name;
    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "FeeBO{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
