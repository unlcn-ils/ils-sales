package com.unlcn.ils.sales.backend.service;

/**
 * Created by houjianhui on 2017/8/14.
 */
public interface OrderAttachService {

    /**
     * 保存销售订单附件
     *
     * @param attach 附件JSON字符串
     * @param saleOrderId 销售订单ID
     * @param saleOrderId 销售订单编码
     * @return
     * @throws Exception
     */
    int save(String attach, Integer saleOrderId, String saleOrderCode) throws Exception;

    /**
     * 根据销售订单ID删除销售订单附件信息
     *
     * @param saleOrderId 销售订单ID
     * @return
     * @throws Exception
     */
    int deleteAttachBySaleOrderId(Integer saleOrderId) throws Exception;
}
