package com.unlcn.ils.sales.api.rest;

import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.sales.api.dto.BrandDTO;
import com.unlcn.ils.sales.backend.bo.BrandBO;
import com.unlcn.ils.sales.backend.service.OrderSkuService;
import io.swagger.annotations.*;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by houjianhui on 2017/6/9.
 */
@Controller
@RequestMapping(value = "/api/brand", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(value = "/brand", description = "商品车")
public class BrandRest {
    private static final Logger LOGGER = LoggerFactory.getLogger(BrandRest.class);

    @Autowired
    private OrderSkuService skuService;

    /**
     * 查询商品车信息
     *
     * @param cvKeyword 查询条件：首字母、全拼、名称
     * @return
     */
    @RequestMapping(value = "/{cvKeyword}", method = RequestMethod.GET)
    @ApiOperation(value = "获取商品车信息", notes = "获取商品车信息", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "listBrand not found")})
    public @ResponseBody
    ResultDTO<Object> listBrand(@ApiParam(value = "查询条件：首字母、全拼、名称", required = false) @PathVariable(value = "cvKeyword") String cvKeyword) {
        ResultDTO<Object> result = new ResultDTO<>(true, null, "查询商品车信息成功");
        try {
            List<BrandDTO> brandDTOS = new ArrayList<>();
            List<BrandBO> list = skuService.listBrand(cvKeyword);
            if (CollectionUtils.isNotEmpty(list)) {
                list.stream().forEach(val -> {
                    BrandDTO dto = new BrandDTO();
                    BeanUtils.copyProperties(val, dto);
                    brandDTOS.add(dto);
                });
            }
            result.setData(brandDTOS);
        } catch (Exception e) {
            LOGGER.error("BrandRest listBrand error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }
}
