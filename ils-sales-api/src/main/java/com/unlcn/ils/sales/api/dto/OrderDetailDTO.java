package com.unlcn.ils.sales.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by houjianhui on 2017/6/9.
 */
@ApiModel(value = "OrderDetailDTO", description = "销售订单明细")
public class OrderDetailDTO {

    // 销售订单表
    @ApiModelProperty(value = "主键ID")
    private Integer id;
    @ApiModelProperty(value = "订单号")
    private String code;
    @ApiModelProperty(value = "下单人")
    private Integer userId;
    @ApiModelProperty(value = "提车时间")
    private Date pickDate;
    @ApiModelProperty(value = "品牌数量")
    private Integer brands;
    @ApiModelProperty(value = "商品车数量")
    private Integer seriesAmt;
    @ApiModelProperty(value = "标准售价")
    private BigDecimal standardSalePrice;
    @ApiModelProperty(value = "标准单公里运价")
    private BigDecimal standardUnitPrice;
    @ApiModelProperty(value = "销售人员建议价格")
    private BigDecimal suggestSalePrice;
    @ApiModelProperty(value = "成交售价")
    private BigDecimal finalSalePrice;
    @ApiModelProperty(value = "采购价格")
    private BigDecimal finalPurchasePrice;
    @ApiModelProperty(value = "销售订单物流状态")
    private Integer orderStatus;
    @ApiModelProperty(value = "销售订单审核状态")
    private Integer auditStatus;
    @ApiModelProperty(value = "备注")
    private String comment;
    @ApiModelProperty(value = "审核备注")
    private String auditComment;
    @ApiModelProperty(value = "销售提成")
    private String pctPrice;


    // 销售订单地址表
    @ApiModelProperty(value = "起运地编码")
    private String departRegionCode;
    @ApiModelProperty(value = "起运地名称")
    private String departRegionName;
    @ApiModelProperty(value = "发车联系人")
    private String departContact;
    @ApiModelProperty(value = "发车联系电话")
    private String departPhone;
    @ApiModelProperty(value = "目的地编码")
    private String destRegionCode;
    @ApiModelProperty(value = "目的地名称")
    private String destRegionName;
    @ApiModelProperty(value = "收车联系人")
    private String destContact;
    @ApiModelProperty(value = "收车人联系电话")
    private String destPhone;
    @ApiModelProperty(value = "里程")
    private BigDecimal distance;

    // 销售订单商品车表
    @ApiModelProperty(value = "商品车信息")
    private List<OrderSkuDTO> orderSkus;

    @ApiModelProperty(value = "销售订单附件信息")
    private List<OrderAttachDTO> attachs;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getPickDate() {
        return pickDate;
    }

    public void setPickDate(Date pickDate) {
        this.pickDate = pickDate;
    }

    public Integer getBrands() {
        return brands;
    }

    public void setBrands(Integer brands) {
        this.brands = brands;
    }

    public Integer getSeriesAmt() {
        return seriesAmt;
    }

    public void setSeriesAmt(Integer seriesAmt) {
        this.seriesAmt = seriesAmt;
    }

    public BigDecimal getStandardSalePrice() {
        return standardSalePrice;
    }

    public void setStandardSalePrice(BigDecimal standardSalePrice) {
        this.standardSalePrice = standardSalePrice;
    }

    public BigDecimal getStandardUnitPrice() {
        return standardUnitPrice;
    }

    public void setStandardUnitPrice(BigDecimal standardUnitPrice) {
        this.standardUnitPrice = standardUnitPrice;
    }

    public BigDecimal getSuggestSalePrice() {
        return suggestSalePrice;
    }

    public void setSuggestSalePrice(BigDecimal suggestSalePrice) {
        this.suggestSalePrice = suggestSalePrice;
    }

    public BigDecimal getFinalSalePrice() {
        return finalSalePrice;
    }

    public void setFinalSalePrice(BigDecimal finalSalePrice) {
        this.finalSalePrice = finalSalePrice;
    }

    public BigDecimal getFinalPurchasePrice() {
        return finalPurchasePrice;
    }

    public void setFinalPurchasePrice(BigDecimal finalPurchasePrice) {
        this.finalPurchasePrice = finalPurchasePrice;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDepartRegionCode() {
        return departRegionCode;
    }

    public void setDepartRegionCode(String departRegionCode) {
        this.departRegionCode = departRegionCode;
    }

    public String getDepartRegionName() {
        return departRegionName;
    }

    public void setDepartRegionName(String departRegionName) {
        this.departRegionName = departRegionName;
    }

    public String getDepartContact() {
        return departContact;
    }

    public void setDepartContact(String departContact) {
        this.departContact = departContact;
    }

    public String getDepartPhone() {
        return departPhone;
    }

    public void setDepartPhone(String departPhone) {
        this.departPhone = departPhone;
    }

    public String getDestRegionCode() {
        return destRegionCode;
    }

    public void setDestRegionCode(String destRegionCode) {
        this.destRegionCode = destRegionCode;
    }

    public String getDestRegionName() {
        return destRegionName;
    }

    public void setDestRegionName(String destRegionName) {
        this.destRegionName = destRegionName;
    }

    public String getDestContact() {
        return destContact;
    }

    public void setDestContact(String destContact) {
        this.destContact = destContact;
    }

    public String getDestPhone() {
        return destPhone;
    }

    public void setDestPhone(String destPhone) {
        this.destPhone = destPhone;
    }

    public BigDecimal getDistance() {
        return distance;
    }

    public void setDistance(BigDecimal distance) {
        this.distance = distance;
    }

    public List<OrderSkuDTO> getOrderSkus() {
        return orderSkus;
    }

    public void setOrderSkus(List<OrderSkuDTO> orderSkus) {
        this.orderSkus = orderSkus;
    }

    public String getAuditComment() {
        return auditComment;
    }

    public void setAuditComment(String auditComment) {
        this.auditComment = auditComment;
    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getPctPrice() {
        return pctPrice;
    }

    public void setPctPrice(String pctPrice) {
        this.pctPrice = pctPrice;
    }

    public List<OrderAttachDTO> getAttachs() {
        return attachs;
    }

    public void setAttachs(List<OrderAttachDTO> attachs) {
        this.attachs = attachs;
    }

    @Override
    public String toString() {
        return "OrderDetailDTO{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", userId=" + userId +
                ", pickDate=" + pickDate +
                ", brands=" + brands +
                ", seriesAmt=" + seriesAmt +
                ", standardSalePrice=" + standardSalePrice +
                ", standardUnitPrice=" + standardUnitPrice +
                ", suggestSalePrice=" + suggestSalePrice +
                ", finalSalePrice=" + finalSalePrice +
                ", finalPurchasePrice=" + finalPurchasePrice +
                ", orderStatus=" + orderStatus +
                ", auditStatus=" + auditStatus +
                ", comment='" + comment + '\'' +
                ", auditComment='" + auditComment + '\'' +
                ", pctPrice='" + pctPrice + '\'' +
                ", departRegionCode='" + departRegionCode + '\'' +
                ", departRegionName='" + departRegionName + '\'' +
                ", departContact='" + departContact + '\'' +
                ", departPhone='" + departPhone + '\'' +
                ", destRegionCode='" + destRegionCode + '\'' +
                ", destRegionName='" + destRegionName + '\'' +
                ", destContact='" + destContact + '\'' +
                ", destPhone='" + destPhone + '\'' +
                ", distance=" + distance +
                ", orderSkus=" + orderSkus +
                ", attachs=" + attachs +
                '}';
    }
}
