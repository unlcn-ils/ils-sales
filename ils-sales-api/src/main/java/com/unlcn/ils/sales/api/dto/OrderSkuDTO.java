package com.unlcn.ils.sales.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Created by houjianhui on 2017/6/9.
 */
@ApiModel(value = "OrderSkuDTO", description = "销售订单商品车信息")
public class OrderSkuDTO {
    @ApiModelProperty(value = "品牌名称")
    private String brandName;
    @ApiModelProperty(value = "车系名称")
    private String seriesName;
    @ApiModelProperty(value = "车型名称")
    private String modelName;
    @ApiModelProperty(value = "台数")
    private Integer amt;
    @ApiModelProperty(value = "车架号")
    private String vin;

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getSeriesName() {
        return seriesName;
    }

    public void setSeriesName(String seriesName) {
        this.seriesName = seriesName;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public Integer getAmt() {
        return amt;
    }

    public void setAmt(Integer amt) {
        this.amt = amt;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    @Override
    public String toString() {
        return "OrderSkuDTO{" +
                "brandName='" + brandName + '\'' +
                ", seriesName='" + seriesName + '\'' +
                ", modelName='" + modelName + '\'' +
                ", amt=" + amt +
                ", vin='" + vin + '\'' +
                '}';
    }
}

