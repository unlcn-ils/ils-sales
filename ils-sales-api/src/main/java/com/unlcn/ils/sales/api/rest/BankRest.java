package com.unlcn.ils.sales.api.rest;

import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.sales.api.dto.UserBankcardDTO;
import com.unlcn.ils.sales.backend.bo.UserBankcardBO;
import com.unlcn.ils.sales.backend.service.UserBankcardService;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by houjianhui on 2017/6/13.
 */
@Controller
@RequestMapping(value = "/api/bank", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(value = "/bank", description = "银行卡")
public class BankRest {

    private static final Logger LOGGER = LoggerFactory.getLogger(BannerRest.class);

    @Autowired
    private UserBankcardService bankcardService;

    /**
     * 查询银行卡列表
     *
     * @return
     */
    @RequestMapping(value = "/bank", method = RequestMethod.GET)
    @ApiOperation(value = "查询银行卡列表", notes = "查询银行卡列表", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "list not found")})
    @ResponseBody
    public ResultDTO<Object> list() {
        ResultDTO<Object> result = new ResultDTO<>(true, null, "查询银行卡列表成功");
        try {
            result.setData(bankcardService.listUserBankcard());
        } catch (Exception e) {
            LOGGER.error("BankRest list error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * 保存银行卡信息
     *
     * @return
     */
    @RequestMapping(value = "/bind", method = RequestMethod.POST)
    @ApiOperation(value = "保存银行卡信息", notes = "保存银行卡信息", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "save not found")})
    @ResponseBody
    public ResultDTO<Object> save(UserBankcardDTO dto) {
        ResultDTO<Object> result = new ResultDTO<>(true, null, "保存银行卡信息成功");
        try {
            UserBankcardBO bo = new UserBankcardBO();
            BeanUtils.copyProperties(dto, bo);
            result.setData(bankcardService.save(bo));
        } catch (Exception e) {
            LOGGER.error("BankRest save error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * 校验银行卡类型
     *
     * @return
     */
    @RequestMapping(value = "/check/{card}", method = RequestMethod.POST)
    @ApiOperation(value = "校验银行卡类型", notes = "校验银行卡类型", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "check not found")})
    @ResponseBody
    public ResultDTO<Object> check(@ApiParam(value = "银行卡卡号", required = true) @PathVariable(value = "card") String card) {
        ResultDTO<Object> result = new ResultDTO<>(true, null, "校验银行卡类型成功");
        try {
            result.setData(bankcardService.checkBankCard(card));
        } catch (Exception e) {
            LOGGER.error("BankRest check error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }


}
