package com.unlcn.ils.sales.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by houjianhui on 2017/6/8.
 */
@ApiModel(value = "OrderDTO", description = "销售订单")
public class OrderDTO {
    @ApiModelProperty(value = "主键ID")
    private Integer id;
    @ApiModelProperty(value = "订单编码")
    private String code;
    @ApiModelProperty(value = "下单人")
    private Integer userId;
    @ApiModelProperty(value = "提车时间")
    private Date pickDate;
    @ApiModelProperty(value = "订单物流状态")
    private Integer orderStatus;
    @ApiModelProperty(value = "销售订单建议价格")
    private BigDecimal suggestSalePrice;
    @ApiModelProperty(value = "订单附件审核状态")
    private Integer attachAuditStatus;
    @ApiModelProperty(value = "订单审核状态")
    private Integer auditStatus;
    @ApiModelProperty(value = "起运地名称")
    private String departRegionName;
    @ApiModelProperty(value = "发车联系人")
    private String departContact;
    @ApiModelProperty(value = "发车人联系电话")
    private String departPhone;
    @ApiModelProperty(value = "目的地名称")
    private String destRegionName;
    @ApiModelProperty(value = "收车联系人")
    private String destContact;
    @ApiModelProperty(value = "收车人联系电话")
    private String destPhone;
    @ApiModelProperty(value = "商品车描述")
    private String brandSeries;
    @ApiModelProperty(value = "订单物流显示状态")
    private String orderStatusText;
    @ApiModelProperty(value = "订单附件审核显示状态")
    private String attachAuditStatusText;

    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getPickDate() {
        return pickDate;
    }

    public void setPickDate(Date pickDate) {
        this.pickDate = pickDate;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getDepartRegionName() {
        return departRegionName;
    }

    public void setDepartRegionName(String departRegionName) {
        this.departRegionName = departRegionName;
    }

    public String getDepartContact() {
        return departContact;
    }

    public void setDepartContact(String departContact) {
        this.departContact = departContact;
    }

    public String getDepartPhone() {
        return departPhone;
    }

    public void setDepartPhone(String departPhone) {
        this.departPhone = departPhone;
    }

    public String getDestRegionName() {
        return destRegionName;
    }

    public void setDestRegionName(String destRegionName) {
        this.destRegionName = destRegionName;
    }

    public String getDestContact() {
        return destContact;
    }

    public void setDestContact(String destContact) {
        this.destContact = destContact;
    }

    public String getDestPhone() {
        return destPhone;
    }

    public void setDestPhone(String destPhone) {
        this.destPhone = destPhone;
    }

    public String getBrandSeries() {
        return brandSeries;
    }

    public void setBrandSeries(String brandSeries) {
        this.brandSeries = brandSeries;
    }

    public String getOrderStatusText() {
        return orderStatusText;
    }

    public void setOrderStatusText(String orderStatusText) {
        this.orderStatusText = orderStatusText;
    }

    public Integer getAttachAuditStatus() {
        return attachAuditStatus;
    }

    public void setAttachAuditStatus(Integer attachAuditStatus) {
        this.attachAuditStatus = attachAuditStatus;
    }

    public String getAttachAuditStatusText() {
        return attachAuditStatusText;
    }

    public void setAttachAuditStatusText(String attachAuditStatusText) {
        this.attachAuditStatusText = attachAuditStatusText;
    }

    public BigDecimal getSuggestSalePrice() {
        return suggestSalePrice;
    }

    public void setSuggestSalePrice(BigDecimal suggestSalePrice) {
        this.suggestSalePrice = suggestSalePrice;
    }

    @Override
    public String toString() {
        return "OrderDTO{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", userId=" + userId +
                ", pickDate=" + pickDate +
                ", orderStatus=" + orderStatus +
                ", suggestSalePrice=" + suggestSalePrice +
                ", attachAuditStatus=" + attachAuditStatus +
                ", auditStatus=" + auditStatus +
                ", departRegionName='" + departRegionName + '\'' +
                ", departContact='" + departContact + '\'' +
                ", departPhone='" + departPhone + '\'' +
                ", destRegionName='" + destRegionName + '\'' +
                ", destContact='" + destContact + '\'' +
                ", destPhone='" + destPhone + '\'' +
                ", brandSeries='" + brandSeries + '\'' +
                ", orderStatusText='" + orderStatusText + '\'' +
                ", attachAuditStatusText='" + attachAuditStatusText + '\'' +
                '}';
    }
}
