package com.unlcn.ils.sales.api.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

/**
 * Created by houjianhui on 2017/6/8.
 */
@ApiModel(value = "查询销售订单对象", description = "查询销售订单对象")
public class OrderQuery {
    // 销售订单编号
    @ApiModelProperty(value = "销售订单编号")
    private String code;
    // 销售订单物流状态
    @ApiModelProperty(value = "销售订单物流状态")
    private Integer orderStatus;
    // 下单人
    @ApiModelProperty(value = "下单人")
    private Integer userId;
    // 启运地
    @ApiModelProperty(value = "启运地")
    private String departRegionCode;
    // 目的地
    @ApiModelProperty(value = "目的地")
    private String destRegionCode;
    // 提车时间（开始）
    @ApiModelProperty(value = "提车时间（开始）")
    private Date pickDateBegin;
    // 提车时间（结束）
    @ApiModelProperty(value = "提车时间（结束）")
    private Date pickDateEnd;
    // 审核状态
    @ApiModelProperty(value = "审核状态")
    private String auditStatus;
    // 支付状态
    @ApiModelProperty(value = "支付状态")
    private Integer payStatus;
    // 附件审核状态
    @ApiModelProperty(value = "附件审核状态")
    private Integer attachAuditStatus;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getDepartRegionCode() {
        return departRegionCode;
    }

    public void setDepartRegionCode(String departRegionCode) {
        this.departRegionCode = departRegionCode;
    }

    public String getDestRegionCode() {
        return destRegionCode;
    }

    public void setDestRegionCode(String destRegionCode) {
        this.destRegionCode = destRegionCode;
    }

    public Date getPickDateBegin() {
        return pickDateBegin;
    }

    public void setPickDateBegin(Date pickDateBegin) {
        this.pickDateBegin = pickDateBegin;
    }

    public Date getPickDateEnd() {
        return pickDateEnd;
    }

    public void setPickDateEnd(Date pickDateEnd) {
        this.pickDateEnd = pickDateEnd;
    }

    public String getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(String auditStatus) {
        this.auditStatus = auditStatus;
    }

    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }

    public Integer getAttachAuditStatus() {
        return attachAuditStatus;
    }

    public void setAttachAuditStatus(Integer attachAuditStatus) {
        this.attachAuditStatus = attachAuditStatus;
    }

    @Override
    public String toString() {
        return "OrderQuery{" +
                "code='" + code + '\'' +
                ", orderStatus=" + orderStatus +
                ", userId=" + userId +
                ", departRegionCode='" + departRegionCode + '\'' +
                ", destRegionCode='" + destRegionCode + '\'' +
                ", pickDateBegin=" + pickDateBegin +
                ", pickDateEnd=" + pickDateEnd +
                ", auditStatus='" + auditStatus + '\'' +
                ", payStatus=" + payStatus +
                ", attachAuditStatus=" + attachAuditStatus +
                '}';
    }
}
