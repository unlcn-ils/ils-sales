package com.unlcn.ils.sales.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Created by houjianhui on 2017/8/15.
 */
@ApiModel(value = "OrderAttachDTO", description = "销售订单附件")
public class OrderAttachDTO {
    @ApiModelProperty(value = "主键ID")
    private Integer id;
    @ApiModelProperty(value = "图片KEY")
    private String picKey;
    @ApiModelProperty(value = "销售订单ID")
    private Integer saleOrderId;
    @ApiModelProperty(value = "附件地址")
    private String url;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPicKey() {
        return picKey;
    }

    public void setPicKey(String picKey) {
        this.picKey = picKey;
    }

    public Integer getSaleOrderId() {
        return saleOrderId;
    }

    public void setSaleOrderId(Integer saleOrderId) {
        this.saleOrderId = saleOrderId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "OrderAttachDTO{" +
                "id=" + id +
                ", picKey='" + picKey + '\'' +
                ", saleOrderId=" + saleOrderId +
                ", url='" + url + '\'' +
                '}';
    }
}
