package com.unlcn.ils.sales.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Created by houjianhui on 2017/6/9.
 */
@ApiModel(value = "UserDTO", description = "用户信息")
public class UserDTO {
    @ApiModelProperty(value = "主键ID")
    private Integer id;
    @ApiModelProperty(value = "登录名称")
    private String loginName;
    @ApiModelProperty(value = "手机号")
    private String phone;
    @ApiModelProperty(value = "用户类型")
    private Integer type;
    @ApiModelProperty(value = "用户状态")
    private Integer userStatus;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "id=" + id +
                ", loginName='" + loginName + '\'' +
                ", phone='" + phone + '\'' +
                ", type=" + type +
                ", userStatus=" + userStatus +
                '}';
    }
}
