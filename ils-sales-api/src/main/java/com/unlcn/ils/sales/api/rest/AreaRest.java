package com.unlcn.ils.sales.api.rest;

import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.sales.api.dto.AreaDTO;
import com.unlcn.ils.sales.backend.bo.AreaBO;
import com.unlcn.ils.sales.backend.service.OrderAddrService;
import io.swagger.annotations.*;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by houjianhui on 2017/6/9.
 */
@Controller
@RequestMapping(value = "/api/area", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(value = "/area", description = "区域")
public class AreaRest {

    private static final Logger LOGGER = LoggerFactory.getLogger(AreaRest.class);

    @Autowired
    private OrderAddrService addrService;

    /**
     * 查询启运地
     *
     * @return
     */
    @RequestMapping(value = "/departarea", method = RequestMethod.GET)
    @ApiOperation(value = "获取起运区域信息", notes = "获取起运区域信息", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "departarea not found")})
    public @ResponseBody
    ResultDTO<Object> departArea() {
        ResultDTO<Object> result = new ResultDTO<Object>(true, null, "查询起运地成功");
        try {
            result.setData(converAreaBOToAreaDTO(addrService.listAreaByCondition("")));
        } catch (Exception e) {
            LOGGER.error("AreaRest startArea error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * 查询目的地
     *
     * @param provinceId 目的地省ID
     * @return
     */
    @RequestMapping(value = "/destarea/{provinceId}", method = RequestMethod.GET)
    @ApiOperation(value = "获取目的地区域信息", notes = "获取目的地区域信息", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "destarea not found")})
    public @ResponseBody
    ResultDTO<Object> destArea(@ApiParam(value = "目的地省ID", required = true) @PathVariable(value = "provinceId") Integer provinceId) {
        ResultDTO<Object> result = new ResultDTO<Object>(true, null, "查询目的地区域成功");
        try {
            result.setData(converAreaBOToAreaDTO(addrService.listAreaByCondition(provinceId)));
        } catch (Exception e) {
            LOGGER.error("AreaRest destArea error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * 查询目的地省
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/provincearea", method = RequestMethod.GET)
    @ApiOperation(value = "获取目的地省信息", notes = "获取目的地省信息", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "destarea not found")})
    public @ResponseBody
    ResultDTO<Object> provinceArea() {
        ResultDTO<Object> result = new ResultDTO<Object>(true, null, "查询目的地省成功");
        try {
            result.setData(converAreaBOToAreaDTO(addrService.listAreaByCondition()));
        } catch (Exception e) {
            LOGGER.error("AreaRest provinceArea error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    private List<AreaDTO> converAreaBOToAreaDTO(List<AreaBO> list) {
        List<AreaDTO> areaDTOS = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(list)) {
            list.stream().forEach(val -> {
                AreaDTO dto = new AreaDTO();
                BeanUtils.copyProperties(val, dto);
                areaDTOS.add(dto);
            });
            return areaDTOS;
        }
        return null;
    }


}
