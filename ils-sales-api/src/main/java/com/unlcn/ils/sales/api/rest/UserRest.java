package com.unlcn.ils.sales.api.rest;

import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.sales.backend.service.UserService;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by houjianhui on 2017/6/9.
 */
@Controller
@RequestMapping(value = "/api/user", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(value = "/user", description = "用户")
public class UserRest {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserRest.class);

    @Autowired
    private UserService userService;

    /**
     * 用户登录接口
     *
     * @param loginName 登录名称
     * @param pwd 密码
     * @return
     */
    @RequestMapping(value = "/login/{loginName}/{pwd}", method = RequestMethod.POST)
    @ApiOperation(value = "用户登录", notes = "用户登录", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "login not found")})
    public @ResponseBody
    ResultDTO<Object> login(@ApiParam(value = "登录名称", required = true) @PathVariable(value = "loginName") String loginName, @ApiParam(value = "密码", required = true) @PathVariable(value = "pwd") String pwd) {
        ResultDTO<Object> result = new ResultDTO<>(true, null, "登录成功");
        try {
            result.setData(userService.login(loginName, pwd));
        } catch (Exception e) {
            LOGGER.error("UserRest login error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * 获取验证码
     *
     * @param phone 手机号
     * @return
     */
    @RequestMapping(value = "/captcha/{phone}", method = RequestMethod.POST)
    @ApiOperation(value = "发送验证码短信", notes = "发送验证码短信", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "captcha not found")})
    public @ResponseBody
    ResultDTO<Object> captcha(@ApiParam(value = "手机号", required = true) @PathVariable(value = "phone") String phone) {
        ResultDTO<Object> result = new ResultDTO<>(true, null, "登录成功");
        try {
            result.setData(userService.captcha(phone));
        } catch (Exception e) {
            LOGGER.error("UserRest captcha error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * 重置密码
     *
     * @param phone 手机号
     * @param pwd 密码
     * @param authCode 验证码
     * @return
     */
    @RequestMapping(value = "/restpwd", method = RequestMethod.POST)
    @ApiOperation(value = "重置密码", notes = "重置密码", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "captcha not found")})
    public @ResponseBody
    ResultDTO<Object> restPwd(String phone, String pwd, String authCode) {
        ResultDTO<Object> result = new ResultDTO<>(true, null, "重置密码成功");
        try {
            result.setData(userService.restPwd(phone, pwd, authCode));
        } catch (Exception e) {
            LOGGER.error("UserRest restPwd error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * 获取当前登录用户
     *
     * @return
     */
    @RequestMapping(value = "/currentuser", method = RequestMethod.GET)
    @ApiOperation(value = "获取当前登录用户", notes = "获取当前登录用户", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "captcha not found")})
    public @ResponseBody
    ResultDTO<Object> getCurrentUser() {
        ResultDTO<Object> result = new ResultDTO<>(true, null, "获取当前登录用户成功");
        try {
            result.setData(userService.getCurrentUser());
        } catch (Exception e) {
            LOGGER.error("UserRest getCurrentUser error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }
}
