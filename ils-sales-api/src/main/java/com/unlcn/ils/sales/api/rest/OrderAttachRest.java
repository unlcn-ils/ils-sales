package com.unlcn.ils.sales.api.rest;

import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.sales.backend.service.OrderAttachService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by houjianhui on 2017/8/14.
 */
@Controller
@RequestMapping(value = "/api/orderAttach", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(value = "/orderAttach", description = "销售订单附件")
public class OrderAttachRest {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderAttachRest.class);

    @Autowired
    private OrderAttachService orderAttachService;

    /**
     * 保存销售订单附件
     *
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ApiOperation(value = "保存销售订单附件", notes = "保存销售订单附件", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResultDTO<Object> save(@ApiParam(value = "图片", required = true) String attachs, @ApiParam(value = "销售订单ID", required = true) Integer saleOrderId, @ApiParam(value = "销售订单编码", required = true) String saleOrderCode) {
        ResultDTO<Object> result = new ResultDTO<>(true, null, "保存附件成功");
        try {
            result.setData(orderAttachService.save(attachs, saleOrderId, saleOrderCode));
        } catch (Exception e) {
            LOGGER.error("OrderAttachRest save error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }
}
