package com.unlcn.ils.sales.api.rest;

import cn.huiyunche.commons.domain.ResultDTO;
import com.unlcn.ils.sales.backend.service.VersionService;
import com.unlcn.ils.sales.base.model.SaleVersionNote;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by houjianhui on 2017/9/18.
 */
@Controller
@RequestMapping(value = "/api/version", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(value = "/order", description = "销售APP版本控制")
public class VersionRest {

    private static final Logger LOGGER = LoggerFactory.getLogger(VersionRest.class);

    private static final String project = "SALE";

    @Autowired
    private VersionService versionService;

    /**
     * 获取最新的ANDROID版本信息
     * @return
     */
    @RequestMapping(value="/android/latest",method= RequestMethod.GET)
    @ApiOperation(value = "获取最新的ANDROID版本信息", notes = "获取最新的ANDROID版本信息", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResultDTO<SaleVersionNote> latestVersionAndroid(){
        ResultDTO<SaleVersionNote> result = new ResultDTO<SaleVersionNote>(true, null, "查询成功");
        SaleVersionNote versionNote = versionService.getLasterVersionAndroid(project);
        result.setSuccess(true);
        result.setData(versionNote);
        if( versionNote == null){
            result.setSuccess(false);
            result.setMessage("已是最新版本!");
        }
        return result;
    }

    /**
     * 获取最新的IOS版本信息
     * @return
     */
    @RequestMapping(value="/ios/latest",method= RequestMethod.GET)
    @ApiOperation(value = "获取最新的IOS版本信息", notes = "获取最新的IOS版本信息", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResultDTO<SaleVersionNote> latestVersionIOS(){
        ResultDTO<SaleVersionNote> result = new ResultDTO<SaleVersionNote>(true, null, "查询成功");
        SaleVersionNote versionNote = versionService.getLasterVersionIOS(project);
        result.setSuccess(true);
        result.setData(versionNote);
        if( versionNote == null){
            result.setSuccess(false);
            result.setMessage("已是最新版本!");
        }
        return result;
    }
}
