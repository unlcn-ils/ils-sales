package com.unlcn.ils.sales.api.rest;

import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.sales.api.dto.PickPwdDTO;
import com.unlcn.ils.sales.backend.bo.PickPwdBO;
import com.unlcn.ils.sales.backend.service.PickPwdService;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by houjianhui on 2017/6/13.
 */
@Controller
@RequestMapping(value = "/api/pickpwd", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(value = "/pickpwd", description = "提现密码")
public class PickPwdRest {
    private static final Logger LOGGER = LoggerFactory.getLogger(PickPwdRest.class);

    @Autowired
    private PickPwdService pickPwdService;

    /**
     * 是否设置提现密码
     *
     * @return
     */
    @RequestMapping(value = "/issettingpwd", method = RequestMethod.GET)
    @ApiOperation(value = "是否设置提现密码", notes = "是否设置提现密码", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "isSettingPwd not found")})
    @ResponseBody
    public ResultDTO<Object> isSettingPwd() {
        ResultDTO<Object> result = new ResultDTO<>(true, null, "查询是否设置提现密码成功");
        try {
            result.setData(pickPwdService.isSettingPwd());
        } catch (Exception e) {
            LOGGER.error("PickPwdRest isSettingPwd error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * 设置提现密码
     *
     * @param dto 提现密码对象
     * @return
     */
    @RequestMapping(value = "/setting", method = RequestMethod.POST)
    @ApiOperation(value = "设置提现密码", notes = "设置提现密码", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "settingPwd not found")})
    @ResponseBody
    public ResultDTO<Object> settingPwd(PickPwdDTO dto) {
        ResultDTO<Object> result = new ResultDTO<>(true, null, "设置提现密码成功");
        try {
            PickPwdBO bo = new PickPwdBO();
            BeanUtils.copyProperties(dto, bo);
            result.setData(pickPwdService.save(bo));
        } catch (Exception e) {
            LOGGER.error("PickPwdRest settingPwd error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * 校验提现密码
     *
     * @param pwd 密码
     * @return
     */
    @RequestMapping(value = "/verification/{pwd}", method = RequestMethod.GET)
    @ApiOperation(value = "校验提现密码", notes = "校验提现密码", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "verification not found")})
    @ResponseBody
    public ResultDTO<Object> verification(@ApiParam(value = "密码", required = true) @PathVariable(value = "pwd") String pwd) {
        ResultDTO<Object> result = new ResultDTO<>(true, null, "校验提现密码成功");
        try {
            result.setData(pickPwdService.verificationPickPwd(pwd));
        } catch (Exception e) {
            LOGGER.error("PickPwdRest verification error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }

    /**
     * 修改提现密码
     *
     * @param pwd 密码
     * @return
     */
    @RequestMapping(value = "/update/{pwd}", method = RequestMethod.GET)
    @ApiOperation(value = "修改提现密码", notes = "修改提现密码", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "verification not found")})
    @ResponseBody
    public ResultDTO<Object> update(@ApiParam(value = "密码", required = true) @PathVariable(value = "pwd") String pwd) {
        ResultDTO<Object> result = new ResultDTO<>(true, null, "修改提现密码成功");
        try {
            result.setData(pickPwdService.updatePickPwd(pwd));
        } catch (Exception e) {
            LOGGER.error("PickPwdRest verification error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }
}
