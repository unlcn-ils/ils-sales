package com.unlcn.ils.sales.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Created by houjianhui on 2017/6/9.
 */
@ApiModel(value = "BrandDTO",description="商品车车型返回对象")
public class BrandDTO {

    @ApiModelProperty(value="车型id")
    private Integer modelId;

    @ApiModelProperty(value="车型编码")
    private String modelCode;

    @ApiModelProperty(value="车型名称")
    private String modelName;

    @ApiModelProperty(value="车系ID")
    private Integer seriesId;

    @ApiModelProperty(value="车系编码")
    private String seriesCode;

    @ApiModelProperty(value="车系名称")
    private String seriesName;

    @ApiModelProperty(value="品牌ID")
    private Integer brandId;

    @ApiModelProperty(value="品牌编码")
    private String brandCode;

    @ApiModelProperty(value="品牌名称")
    private String brandName;

    @ApiModelProperty(value="品牌LogoUrl")
    private String brandLogoUrl;

    public Integer getModelId() {
        return modelId;
    }

    public void setModelId(Integer modelId) {
        this.modelId = modelId;
    }

    public String getModelCode() {
        return modelCode;
    }

    public void setModelCode(String modelCode) {
        this.modelCode = modelCode;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public Integer getSeriesId() {
        return seriesId;
    }

    public void setSeriesId(Integer seriesId) {
        this.seriesId = seriesId;
    }

    public String getSeriesCode() {
        return seriesCode;
    }

    public void setSeriesCode(String seriesCode) {
        this.seriesCode = seriesCode;
    }

    public String getSeriesName() {
        return seriesName;
    }

    public void setSeriesName(String seriesName) {
        this.seriesName = seriesName;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public String getBrandCode() {
        return brandCode;
    }

    public void setBrandCode(String brandCode) {
        this.brandCode = brandCode;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getBrandLogoUrl() {
        return brandLogoUrl;
    }

    public void setBrandLogoUrl(String brandLogoUrl) {
        this.brandLogoUrl = brandLogoUrl;
    }

    @Override
    public String toString() {
        return "BrandDTO{" +
                "modelId=" + modelId +
                ", modelCode='" + modelCode + '\'' +
                ", modelName='" + modelName + '\'' +
                ", seriesId=" + seriesId +
                ", seriesCode='" + seriesCode + '\'' +
                ", seriesName='" + seriesName + '\'' +
                ", brandId=" + brandId +
                ", brandCode='" + brandCode + '\'' +
                ", brandName='" + brandName + '\'' +
                ", brandLogoUrl='" + brandLogoUrl + '\'' +
                '}';
    }
}
