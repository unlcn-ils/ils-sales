package com.unlcn.ils.sales.api.rest;

import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.sales.backend.service.BonusBalanceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by houjianhui on 2017/6/13.
 */
@Controller
@RequestMapping(value = "/api/balance", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(value = "/balance", description = "钱包")
public class BalanceRest {
    private static final Logger LOGGER = LoggerFactory.getLogger(BalanceRest.class);

    @Autowired
    private BonusBalanceService bonusBalanceService;

    /**
     * 获取钱包余额
     *
     * @return
     */
    @RequestMapping(value = "/surplus", method = RequestMethod.GET)
    @ApiOperation(value = "获取钱包余额", notes = "获取钱包余额", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "surplus not found")})
    public @ResponseBody
    ResultDTO<Object> surplus() {
        ResultDTO<Object> result = new ResultDTO<>(true, null, "获取钱包余额成功");
        try {
            result.setData(bonusBalanceService.getUserBalanceByUserId());
        } catch (Exception e) {
            LOGGER.error("BalanceRest surplus error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }
}
