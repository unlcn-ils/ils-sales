package com.unlcn.ils.sales.api.rest;

import cn.huiyunche.commons.domain.ResultDTO;
import cn.huiyunche.commons.exception.BusinessException;
import com.unlcn.ils.sales.backend.service.BannerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by houjianhui on 2017/6/10.
 */
@Controller
@RequestMapping(value = "/api/banner", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(value = "/banner", description = "轮播图")
public class BannerRest {
    private static final Logger LOGGER = LoggerFactory.getLogger(BannerRest.class);

    @Autowired
    private BannerService bannerService;

    /**
     * 查询轮播图
     *
     * @param clientType 客户端类型
     * @param appType 应用类型
     * @param w 宽度
     * @param h 高度
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiOperation(value = "查询轮播图", notes = "查询轮播图", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "listBanner not found")})
    public @ResponseBody
    ResultDTO<Object> listBanner(String clientType, String appType, String w, String h) {
        ResultDTO<Object> result = new ResultDTO<>(true, null, "查询轮播图成功");
        try {
            result.setData(bannerService.listBanner(clientType, appType, w, h));
        } catch (Exception e) {
            LOGGER.error("BannerRest listBanner error: {}", e);
            throw new BusinessException(e.getMessage());
        }
        return result;
    }
}
